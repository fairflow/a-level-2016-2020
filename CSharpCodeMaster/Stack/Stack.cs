﻿using System;

namespace Stack
{
    class Program
    {
        public class Stack<Type> // A class implementing a stack
        {// we can have stacks of many different types: hence the <Type>
            private int maxSize;
            private int currentSize;
            private int top;
            private Type[] stacker;

            public Stack(int max)
            {
                Console.WriteLine("New stack of size {0} created", max);
                stacker = new Type[max];
                maxSize = max;
                currentSize = 0;
                top = 0; // top points to the next free location
            }

            public Stack(int max, Type[] start)
            {
                var startSize = start.Length;
                Console.WriteLine("Stack of size {0} created from array", startSize);
                stacker = start;
                maxSize = max;
                currentSize = startSize;
                top = currentSize; // top points to the next free location counting from 0
            }

            public void Push(Type item)
            {
                if (!Full())
                {
                    currentSize++;
                    stacker[top] = item; // top points to the next free location
                    top++;
                }
                else Console.WriteLine("Stack full.  Sorry!");
            }

            public Type Pop()
            {
                if (!Empty())
                {
                    currentSize--;
                    top--;
                    // Console.WriteLine("Top is: {0}", top);
                    var item = stacker[top];
                    return item;
                }
                else
                {
                    Console.WriteLine("Stack empty.  What to do?");
                    return default; // in C# every type has a default value
                }
            }

            public bool Empty()
            {
                return (currentSize == 0);
            }

            public bool Full()
            {
                return (currentSize >= maxSize);
            }

            public Type[] GetItems()
            {
                return stacker; // Exercise: is anything wrong with this?
            }

            public void PrintItems()
            {
                for (var i = 0; i < currentSize; i++)
                { Console.WriteLine(stacker[i]); }
            }
        }

        public static void Main()
        {
            Console.WriteLine("Demonstrating own stack datatype\n");

            Stack<string> animals = new Stack<string>(5);

            animals.Push("manatee");
            animals.Push("pig");
            animals.Push("chicken");
            animals.Push("fruit-fly");
            animals.Push("alligator");

            Console.WriteLine("Using our method for getting all items for enumeration:");
            foreach (string animal in animals.GetItems())
            {
                Console.WriteLine(animal);
            }

            Console.WriteLine("\nTesting Pop\n");
            Console.WriteLine("Popping '{0}'", animals.Pop());
            Console.WriteLine("Popping '{0}'", animals.Pop());

            if (animals.Empty())
            {
                Console.WriteLine("Animal stack is empty");
            }
            else
            if (animals.Full())
            {
                Console.WriteLine("Animal stack is full");
            }
            else
            {
                Console.WriteLine("Animal stack is just fine");
            }

            animals.Push("pigeon");
            animals.Push("diplodocus");
            Console.WriteLine("\nInspect stack again:");
            foreach (string animal in animals.GetItems())
            {
                Console.WriteLine(animal);
            }

            if (animals.Empty())
            {
                Console.WriteLine("Animal stack is empty");
            }
            else
            if (animals.Full())
            {
                Console.WriteLine("Animal stack is full");
            }
            else
            {
                Console.WriteLine("Animal stack is just fine");
            }

            Console.WriteLine("\nNow pop five items...");
            animals.Pop();
            animals.Pop();
            animals.Pop();
            animals.Pop();
            animals.Pop();
            if (animals.Empty())
            {
                Console.WriteLine("Animal stack is empty");
            }
            else
            if (animals.Full())
            {
                Console.WriteLine("Animal stack is full");
            }
            else
            {
                Console.WriteLine("Animal stack is just fine");
            }

            Console.WriteLine("\nTesting creating a stack from an array:");
            Stack<string> characters =
                new Stack<string>(7, new string[] { "a", "b", "c", "d", "e", "f" });

            characters.PrintItems();
            Console.WriteLine("\nExercises");
            Console.WriteLine("*********");
            Console.WriteLine("1. What order will these items be popped in?");
            Console.WriteLine("2. And what problems could arise with " +
                "the definition of this constructor?");
            Console.WriteLine("3. Why is a pointer not needed to the bottom " +
                "of the stack?");
            
            Console.WriteLine("\nNow to testing another stack...");
            var orderStack = new Stack<string>(5);
            orderStack.Push("Order45");
            orderStack.Push("Order38");
            orderStack.Push("Order92");
            Console.WriteLine("Get all items for enumeration:");
            foreach (string order in orderStack.GetItems())
            {
                Console.WriteLine(order);
            }

            Console.WriteLine("\nTesting Pop\n");
            Console.WriteLine("Popping '{0}'", orderStack.Pop());
            Console.WriteLine("Popping '{0}'", orderStack.Pop());
            Console.WriteLine("Popping '{0}'", orderStack.Pop());

            orderStack.Push("Order44");
            orderStack.Push("Order55");
            orderStack.Push("Order66");
            orderStack.Push("Order77");

            Console.WriteLine("Popping '{0}'", orderStack.Pop());

            Console.WriteLine("Get all items for enumeration:");
            foreach (string order in orderStack.GetItems())
            {
                Console.WriteLine(order);
            }
            Console.WriteLine("Print all items:");
            orderStack.PrintItems();

            Console.WriteLine("\nFinished with Stacks altogether!");
        }
    }
}
