﻿﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Priority
{
    public class Range
    {
        public int Minimum { get; set; }
        public int Maximum { get; set; }
        public Range(int lower, int upper)
        {
            if (lower <= upper)
            {
                Minimum = lower;
                Maximum = upper;
            }
            else
            {
                Minimum = 0;
                Maximum = 0;
            }
        }
    }

    public class PriorityQueue<ElementType>
    {
        private Dictionary<int, CircularQueue<ElementType>> PriorityQ;
        // The main data structure for the queue: essentially a list of queues
        private Range Levels;    // stores the minimum and maximum permitted priority levels
        private int MaxWidth;    // maximum number of queues: determined by Levels
        private int CurrentSize; // current number of elements in total
        private int MaxLength;   // maximum length of individual queues

        public PriorityQueue(Range priorityLevels, int maxQueueLength)
        {
            PriorityQ = new Dictionary<int, CircularQueue<ElementType>>();
            Levels = priorityLevels;
            MaxWidth = Levels.Maximum - Levels.Minimum + 1;
            MaxLength = maxQueueLength;
            CurrentSize = 0;
        }

        public void EnQueue(int order, ElementType item) // reusing method name
        {
            var keys = PriorityQ.Keys;
            if (Levels.Minimum <= order && order <= Levels.Maximum)
                if (!keys.Contains(order)) // we have no elements with this priority level
                {
                    Console.WriteLine("Adding item {0} to fresh queue at priority {1}", item, order);
                    CurrentSize++;
                    var freshQueue = new CircularQueue<ElementType>(MaxLength);
                    freshQueue.EnQueue(item);
                    PriorityQ.Add(order, freshQueue);
                }
                else // we already have elements queued at this priority
                {
                    if (!PriorityQ[order].Full()) // there is room at this order
                    {
                        Console.WriteLine("Adding item {0} to existing queue at priority {1}", item, order);
                        CurrentSize++;
                        PriorityQ[order].EnQueue(item);
                    }
                    else { Console.WriteLine("No room in queue!"); }
                }
            else
            { Console.WriteLine("Priority value {0} outside allowed range.", order); }
        }

        public ElementType DeQueue()
        {
            var keys = PriorityQ.Keys;
            var order = keys.Min(); // will this succeed if there are no keys?
            if (keys.Contains(order) && !PriorityQ[order].Empty())
            {
                CurrentSize--;
                var newQueue = PriorityQ[order]; // retrieve the entire queue
                var newElement = newQueue.DeQueue(); // dequeue and store resuilt
                if (!newQueue.Empty())
                { PriorityQ[order] = newQueue; // update the priority queue with shorter queue
                }
                else
                { PriorityQ.Remove(order); // no more items with this priority so remove queue
                }
                return newElement; // we are done
            }
            Console.WriteLine("Nothing to dequeue.");
            return default;
        }

        public int QueueSize()
        { return CurrentSize; }

        public Range GetRange()
        { return Levels; }

        public bool Empty()
        { return QueueSize() == 0; }

        public bool Full()
        { return (CurrentSize >= MaxWidth * MaxLength); }
        // Note that the queue can have room but not necessarily at every priority
        // Exercise: design an algorithm to return capacities at each priority
        // Now adapt your code to calculate the actual total capacity ignoring priorities

        public List<ElementType> GetItems() // we use a list as nothing needs to be modifified
        {
            var gathered = new List<ElementType>(); // initially empty
            List<ElementType> currentItems;
            foreach (int order in PriorityQ.Keys)
            {
                currentItems = PriorityQ[order].GetItems().ToList();
                gathered = gathered.Concat(currentItems).ToList(); // Linq needed for Concat
            }
            // var toReturn = gathered.ToArray(); // not this one
            return gathered; // strange but true: we need to convert
        }
    }
}
