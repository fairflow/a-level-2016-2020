﻿using System;

namespace Priority
{
    class Program
    {
        public static void Main()
        {
            Console.WriteLine("Demonstrating Priority Queue datatype\n");

            Range priorityLevels = new Range(0, 3);
            var mimi = priorityLevels.Minimum;
            var maxi = priorityLevels.Maximum;
            Console.WriteLine("Levels range from {0} to {1}", mimi, maxi);

            PriorityQueue<string> animals = new PriorityQueue<string>(priorityLevels, 5);

            Range animalPriorityRange = animals.GetRange(); // should be same as priorityLevels
            int potentialWidth = animalPriorityRange.Maximum - animalPriorityRange.Minimum + 1;
            Console.WriteLine("New priority queue of potential width {0} created", potentialWidth);

            foreach (string animal in animals.GetItems())
            {
                Console.WriteLine(animal);
            }

            animals.EnQueue(1, "manatee");
            animals.EnQueue(1, "pig");
            animals.EnQueue(3, "chicken");
            animals.EnQueue(0, "fruit-fly");
            animals.EnQueue(3, "alligator");

            Console.WriteLine("Using our method for getting all items for enumeration:");
            foreach (string animal in animals.GetItems())
            {
                Console.WriteLine(animal);
            }

            Console.WriteLine("\nTesting DeQueue\n");
            Console.WriteLine("Dequeuing '{0}'", animals.DeQueue());
            Console.WriteLine("Dequeuing '{0}'", animals.DeQueue());
            Console.WriteLine("Dequeuing '{0}'", animals.DeQueue());

            if (animals.Empty())
            {
                Console.WriteLine("Animal queue is empty");
            }
            else
            if (animals.Full())
            {
                Console.WriteLine("Animal queue is full");
            }
            else
            {
                Console.WriteLine("Animal queue is just fine");
            }

            animals.EnQueue(2, "pigeon");
            animals.EnQueue(0, "diplodocus");
            animals.EnQueue(5, "this should fail!");

            Console.WriteLine("\nInspect queue again:");
            foreach (string animal in animals.GetItems())
            {
                Console.WriteLine(animal);
            }

            if (animals.Empty())
            {
                Console.WriteLine("Animal queue is empty");
            }
            else
            if (animals.Full())
            {
                Console.WriteLine("Animal queue is full");
            }
            else
            {
                Console.WriteLine("Animal queue is just fine");
            }

            Console.WriteLine("\nNow dequeue all items...");
            Console.WriteLine("{0}...", animals.DeQueue());
            Console.WriteLine("{0}...", animals.DeQueue());
            Console.WriteLine("{0}...", animals.DeQueue());
            Console.WriteLine("{0}...", animals.DeQueue());

            if (animals.Empty())
            {
                Console.WriteLine("Animal queue is empty");
            }
            else
            if (animals.Full())
            {
                Console.WriteLine("Animal queue is full");
            }
            else
            {
                Console.WriteLine("Animal queue is just fine");
            }

            Console.WriteLine("\nTesting creating a queue from an array:");

            CircularQueue<string> characters =
                new CircularQueue<string>(10, new string[] { "a", "b", "c", "d", "e" });
            foreach (string character in characters.GetItems())
            {
                Console.WriteLine(character);
            }

            Console.WriteLine("\nDone with animal and character queues now!");

            Console.WriteLine("\nNow to testing a circular queue...");
            var printQueue = new CircularQueue<string>(5);
            printQueue.EnQueue("J45");
            printQueue.EnQueue("J38");
            printQueue.EnQueue("J92");
            Console.WriteLine("Get all items for enumeration:");
            foreach (string job in printQueue.GetItems())
            {
                Console.WriteLine(job);
            }

            Console.WriteLine("\nTesting DeQueue\n");
            Console.WriteLine("Dequeuing '{0}'", printQueue.DeQueue());
            Console.WriteLine("Dequeuing '{0}'", printQueue.DeQueue());
            Console.WriteLine("Dequeuing '{0}'", printQueue.DeQueue());

            printQueue.EnQueue("J44");
            printQueue.EnQueue("J55");
            printQueue.EnQueue("J66");
            printQueue.EnQueue("J77");

            Console.WriteLine("Dequeuing '{0}'", printQueue.DeQueue());

            Console.WriteLine("Get all items for enumeration:");
            foreach (string job in printQueue.GetItems())
            {
                Console.WriteLine(job);
            }
            Console.WriteLine("Print all items:");
            printQueue.PrintItems();

            Console.WriteLine("\nFinished with Queues altogether!");
        }
    }
}
