﻿using System;
namespace Priority
{

    public class CircularQueue<Type> // A class implementing a circular queue
    {// we can have queues of many different types: hence the <Type>
        private int maxSize;
        private int currentSize;
        private int front;
        private int back;
        private Type[] queue;

        public CircularQueue(int max)
        {
            Console.WriteLine("New queue of size {0} created", max);
            queue = new Type[max];
            maxSize = max;
            currentSize = 0;
            front = 0;
            back = 0; // back points to the next free location
        }

        public CircularQueue(int max, Type[] array)
        {
            Console.WriteLine("New queue of size {0} created", max);
            queue = array;
            maxSize = max;
            currentSize = array.Length;
            front = 0;
            back = 0; // back points to the next free location
        }

        public void EnQueue(Type item)
        {
            if (!Full())
            {
                currentSize++;
                queue[back] = item; // back points to the next free location
                back = (back + 1) % maxSize;
            }
            else Console.WriteLine("Queue full.  Sorry!");
        }

        public Type DeQueue()
        {
            if (!Empty())
            {
                var item = queue[front];
                front = (front + 1) % maxSize;
                currentSize--;
                return item;
            }
            else
            {
                Console.WriteLine("Queue empty.  What to do?");
                return default; // in C# every type has a default value
            }
        }

        public bool Empty()
        {
            return (currentSize == 0);
        }

        public bool Full()
        {
            return (currentSize >= maxSize);
        }

        public Type[] GetItems()
        {
            return queue; // Exercise: what is wrong with this?
        }

        public void PrintItems()
        {
            for (var i = front; i != back; i = (i + 1) % maxSize)
            { Console.WriteLine(queue[i]); }
        }
    }
}
