﻿using System;
using System.Globalization;

namespace Vehicle
{
    class Car
    {
        private string Registration;
        private string Make;
        private int Mileage;
        private DateTime DateOfInspection;

        public Car(string r, string m)
        {
            Registration = r;
            Make = m;
            Mileage = 0;
            DateOfInspection = DateTime.Now;
        }

        public string GetRegistration() { return Registration; }
        public string GetMake() { return Make;  }
        public int GetMileage() { return Mileage; }
        public DateTime GetDateOfInspection() { return DateOfInspection; }

        public void SetInspectionData(int m, DateTime d)
        {
            Mileage = m;
            DateOfInspection = d;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Vehicle registration beginning...\n");
            var myCar = new Car("ABC 99", "VW");
            var dateString = "12/04/2016";
            Console.WriteLine("I have a {0} with registration {1}.",
                myCar.GetMake(), myCar.GetRegistration());
            myCar.SetInspectionData(1234, Convert.ToDateTime(dateString));
            Console.WriteLine("My {0} registration {1} was inspected on {2}",
                myCar.GetMake(), myCar.GetRegistration(),
                myCar.GetDateOfInspection().ToShortDateString());
            Console.WriteLine("and has done {0} miles.\n",
                myCar.GetMileage());

            Console.WriteLine("Other possibilities for parsing different dates:");
            CultureInfo provider = CultureInfo.InvariantCulture;
            var format = "dd/MM/yyyy";
            DateTime result;
            try
            {
                result = DateTime.ParseExact(dateString, format, provider);
                Console.WriteLine("{0} converts to {1}.", dateString, result.ToShortDateString());
            }
            catch (FormatException)
            { Console.WriteLine("{0} is not in the correct format.", dateString); }
        }
    }
}
