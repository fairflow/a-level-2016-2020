﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Trees
{
    public class SizeTooSmallException : Exception
    {
        public SizeTooSmallException(string message)
            : base(message)
        { }
    }

    class Trees
    {
        public static void Underline(string message)
        {
            Console.WriteLine(message);
            string output = "";
            for (var i = 0; i < message.Length; i++)
            { output += "=";  }
            Console.WriteLine(output);
        }

        public delegate void TreeVisitor<T>(T nodeData); // ignore this until we cover Functional Programming

        class BinTree<T> // type of binary trees with 2 or fewer children
        {
            private T data; // data at node or leaf
            private BinTree<T> left;  // left sub-tree
            private BinTree<T> right; // right sub-tree

            public BinTree(T d) // make a leaf
            {
                data = d;
                left = null;
                right = null;
            }

            public BinTree(BinTree<T> l, T d, BinTree<T> r) // make a node
            {
                left = l;
                data = d;
                right = r;
            }

            // purely functional variants
            public static BinTree<T> Leaf(T d)
            {
                return new BinTree<T>(d);
            }

            public static BinTree<T> Node(BinTree<T> l, T d, BinTree<T> r)
            {
                return new BinTree<T>(l, d, r);
            }

            public T Value()
            {
                return data;
            }

            public void PreOrder()
            {
                Console.WriteLine("Data: {0}", data);
                if (left != null)
                {
                    left.PreOrder();
                }
                if (right != null)
                {
                    right.PreOrder();
                }
            }

            public void InOrder()
            {
                if (left != null)
                {
                    left.InOrder();
                }
                Console.WriteLine("Data: {0}", data);
                if (right != null)
                {
                    right.InOrder();
                }
            }

            public void PostOrder()
            {
                if (left != null)
                {
                    left.PostOrder();
                }
                if (right != null)
                {
                    right.PostOrder();
                }
                Console.WriteLine("Data: {0}", data);
            }
        }

        public enum Dir
        {
            U, D, L, R, T
        }

        public class VarTree<T> // type of trees with varying numbers of children
        {
            public T data;
            public List<VarTree<T>> children; // make private once code is there
            private List<int> pathToCurrent; // unused

            public VarTree(T data)
            {
                this.data = data;
                children = new List<VarTree<T>>();
                // A leaf is a tree with no children
                pathToCurrent = new List<int>();
            }

            public void AddChild(T data)
            {
                children.Add(new VarTree<T>(data));
            }

            public VarTree<T> GetChild(int i)
            {
                foreach (VarTree<T> n in children)
                    if (--i == 0)
                        return n;
                return null; // no child of that index
            }

            public VarTree<T> Follow(List<int> path)
            {
                VarTree<T> soFar = this;
                foreach (var dir in path)
                {
                    soFar = GetChild(dir);
                }
                return soFar; // a new tree, how do we play this one?
            }

            public void PreOrder(VarTree<T> node)
            {
                Console.WriteLine("Data: {0}", node.data);
                foreach (VarTree<T> kid in node.children)
                    PreOrder(kid);
            }

            public void Traverse(VarTree<T> node, TreeVisitor<T> visitor)
                // ignore Traverse until we cover Functional Programming
            {
                visitor(node.data);
                foreach (VarTree<T> kid in node.children)
                    Traverse(kid, visitor);
            }
        }

        public class MultiSet<T> : IEnumerable<T>
        {
            private Dictionary<T, int> mset;

            public IEnumerator<T> GetEnumerator() // ?MultiSet<T>?
            {
                return mset.Keys.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return mset.Keys.GetEnumerator();
            }

            public MultiSet(T value)
            {   // create singleton multiset
                mset = new Dictionary<T, int>();
                mset.Add(value, 1);
            }
            public MultiSet(List<T> elements)
            {   // create a multiset from a list
                mset = new Dictionary<T, int>();
                foreach (T v in elements)
                {  mset.Add(v, 1);  } // add elements 1 by 1
            }
            public MultiSet(Dictionary<T, int> dict) // take care with types if using
            { mset = dict; }

            public int Multiplicity(T element)
            { return mset.ContainsKey(element) ? mset[element] : 0; }

            public bool Contains(T element)
            { return Multiplicity(element) > 0; }

            public int Size()
            {
                int s = 0;
                foreach (T element in mset.Keys) { s += mset[element]; }
                return s;
            }
            public bool IsEmpty()
            { return Size() == 0; }

            public void Add(T element)
            {
                if (!(mset.TryAdd(element, 1))) // so already in set
                {
                    int count = mset[element];
                    mset[element] = count + 1;
                    // mset.Remove(element); // inefficient 
                    // mset.Add(element, count+1); // unnecessary
                }
            }
            public void Remove(T element)
            {   // this originally did not remove the key if the multiplicity is 1
                // but it avoided creating negative multiplicities
                // but now with crude extraction of keys this no longer works
                int count = Multiplicity(element);
                if (count - 1 > 0)
                {
                    mset[element] = count - 1;
                }
                else
                {
                    mset.Remove(element);
                }
            }
            public void RemoveAll(T element) // this removes key and all multiples
            { mset.Remove(element); }

            public bool Exists(Predicate<T> p)
            {
                return mset.Keys.ToList().Exists(p); // ugly: functional?
                // no: a multiplicity of 0 would wreck this
                // fixed via Remove
            }
            public List<T> FindAll(Predicate<T> p)
            {
                return mset.Keys.ToList().FindAll(p);
            }
        }

        public class SetGame
        {
            private MultiSet<int> state;

            public SetGame(int size)
            {
                state = new MultiSet<int>(size);
            }
            public SetGame(MultiSet<int> start)
            {
                state = start;
            }

            public bool ValidMove(int left, int right)
            {
                var lump = left + right;
                return (left > 0 && right > 0 && left != right && state.Contains(lump));
            }

            public void Move(int left, int right)
            {
                var lump = left + right;
                if (ValidMove(left, right))
                {
                    state.Remove(lump);
                    state.Add(left);
                    state.Add(right);
                    // state.Sort(); // dictionaries, hence multisets are unordered
                }
                else
                {
                    Console.WriteLine("Illegal move in SetGame.Move, sorry.");
                }
            }

            public MultiSet<int> GetState()
            { return state; }

            public string Show()
            {   // illustrates a workable use of foreach on multisets
                string row1 = "lumps:  ";
                string row2 = "counts: ";
                foreach (var lump in state)
                {
                    row1 += lump.ToString();
                    row1 += " "; // add spaces between lumps
                    row2 += state.Multiplicity(lump); // no ToString?
                    row2 += " "; // add spaces between lump multiplicities
                }
                return row1 + "\n" + row2;
            }

            public bool GameOver()
            {
                return (!state.Exists(x => x > 2));
            }
        }

        public class TT<T> // TT for Test Tree make it short
        {
            static bool debugging = true;

            public VarTree<T> tree { get; set; }
            public T node { get; set; }
            private VarTree<T> current;
            public List<int> pathToCurrent { get; set; }

            public TT(T data)
            {   tree = new VarTree<T>(data);
                node = data;
                current = tree;
                pathToCurrent = new List<int>();
            }

            public void Down(int dir)
            {
                current = current.GetChild(dir);
                pathToCurrent.Add(dir);
                if (debugging) // put this somewhere else
                {
                    Console.WriteLine(current.data.ToString());
                }
            }
        }

        public class TreeGame
        {
            private VarTree<SetGame> tree;
            private SetGame node; // needed?
            private VarTree<SetGame> current;
            private List<int> pathToCurrent;

            public TreeGame(int size)
            {
                tree = new VarTree<SetGame>(new SetGame(size));
                current = tree;
                node = tree.data;
                pathToCurrent = new List<int>();
            }
            public TreeGame(SetGame g)
            {
                tree = new VarTree<SetGame>(g);
                current = tree;
                node = tree.data;
                pathToCurrent = new List<int>();
            }

            public List<int> GetPath()
            { return pathToCurrent; }

            public VarTree<SetGame> GetCurrentTree()
            { return current; }

            public SetGame GetCurrentGame()
            { return current.data; }

            public void Down(/* int dir */)
            {
                int dir = 1;  // until we have branching
                current = current.GetChild(dir);
                pathToCurrent.Add(dir);
                if (Play.debugging) // put this somewhere else
                {
                    Console.WriteLine(current.data.Show());
                }
            }

            public void Up() // currently ineffective, check Follow next
            {
                int most = pathToCurrent.Count - 1;
                if (most >= 0)
                {
                    pathToCurrent = pathToCurrent.Take(most).ToList();
                    current = tree.Follow(pathToCurrent);
                }
            } // N.B. this is also very inefficient; why not keep link to parent instead?

            public void Move(int left, int right)
            {
                var copy = new SetGame(current.data.GetState());
                // create a copy of data as a new game
                copy.Move(left, right);
                // make your move

                current.AddChild(copy);
                // make a child node from the move
                // multiple moves create multiple children
                // but that is not yet implemented

                Down();
                // go down to new child (only one as yet)
            }

            public string /*TreeVisitor<SetGame>*/ printer(SetGame g)
            {
                return g.Show();
            }

            public string Show() { return current.data.Show(); }

            public string ShowAll() //whoa, what a mixup!
            {
                string temp = Show() + "\n";
                foreach (VarTree<SetGame> kid in tree.children)
                {
                    temp += kid.data.Show(); // ShowAll?
                }
                temp += "\n";
                return temp;
            }
            public void Show2()
            { tree.Traverse(tree, (SetGame nodeData) => Console.WriteLine(nodeData.Show()));  }
        }

        public class SplitGame
        {
            private List<int> state;

            public SplitGame(int size)
            {
                state = new List<int> { size };
            }
            public SplitGame(List<int> start)
            {
                state = start;
            }
            public bool ValidMove(int left, int right)
            {
                var lump = left + right;
                return (left > 0 && right > 0 && left != right && state.Contains(lump));
            }
            public void Move(int left, int right)
            {
                var lump = left + right;
                if (ValidMove(left, right))
                {
                    state.Remove(lump);
                    state.Add(left);
                    state.Add(right);
                    state.Sort(); // inefficient but simple to code
                }
                else
                {
                    Console.WriteLine("Illegal move, sorry.");
                }
            }

            public List<int> GetState()
            { return state; }

            public string Show()
            {
                string toShow = "";
                foreach (var lump in state)
                {
                    toShow += lump.ToString();
                    toShow += " "; // add spaces between lumps
                }
                return toShow;
            }

            public bool GameOver()
            {
                return (!state.Exists(x => x > 2));
            }
        }

        public abstract class GeneralPlayer
        {
            protected string Name;

            public GeneralPlayer()
            { // no body provided here but a constructor appears to be necessary
            }

            public GeneralPlayer(string myName)
            { Name = myName; }

            public string GetName()
            { return Name; }

            public abstract void NewPair(SplitGame g, out int left, out int right);

            public abstract void MkPair(SetGame g, out int left, out int right);
        }

        public class CleverPlayer : GeneralPlayer
        {
            public CleverPlayer(string n)
            { Name = n; }

            public override void NewPair(SplitGame g, out int left, out int right)
            {
                var cleverMoves = g.GetState(); // some move should be possible
                // cleverMoves.Sort((x, y) => (x > y) ? x : y); // sort in decreasing order??
                // the first test is redundant as a possible move means a lump bigger than 2
                var goodMoves = cleverMoves.FindAll(i => ( i > 2 && ((i % 3) == 0 || (i % 3) == 2)));
                if (goodMoves.Count > 0)
                {
                    // goodMoves.Sort((x, y) => (x < y) ? x : y); // sort in decreasing order??
                    var best = goodMoves.Max();
                    if ((best % 3) == 2)
                    {
                        left = best - 1;
                        right = best - left;
                    }
                    else
                    {
                        left = best - 2;
                        right = best - left;
                    }
                }
                else
                {
                    var guess = cleverMoves.Max();
                    left = (guess == 4) ? 3 : guess / 2 + 1;
                    right = guess - left;
                }
            }

            public override void MkPair(SetGame g, out int left, out int right)
            {
                var cleverMoves = g.GetState(); // some move should be possible

                // the first test is usually redundant as a possible move means a lump bigger than 2
                var goodMoves = cleverMoves.FindAll(i => (i > 2 && ((i % 3) == 0 || (i % 3) == 2)));
                if (goodMoves.Count > 0)
                {
                    var best = goodMoves.Max();
                    if ((best % 3) == 2)
                    {
                        left = best - 1;
                        right = best - left;
                    }
                    else
                    {
                        left = best - 2;
                        right = best - left;
                    }
                }
                else
                {
                    var guess = cleverMoves.Max();
                    left = (guess == 4) ? 3 : guess / 2 + 1;
                    right = guess - left;
                    // very crude stuff now game is better understood
                }
            }
        }

        public class InteractivePlayer : GeneralPlayer
        {
            public InteractivePlayer()
            {
                Console.WriteLine("Enter your name: ");
                Name = Console.ReadLine();
            }

            public override void NewPair(SplitGame g, out int left, out int right)
            {
                do
                {
                    Console.WriteLine("{0}, enter pile size 1: ", Name);
                    left = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter pile size 2: ");
                    right = Convert.ToInt32(Console.ReadLine());
                    if (!g.ValidMove(left, right))
                    {
                        Console.WriteLine("Not a valid move, try again: ");
                    }
                }
                while (!g.ValidMove(left, right));
            }

            public override void MkPair(SetGame g, out int left, out int right)
            {
                do
                {
                    Console.WriteLine("{0}, enter pile size 1: ", Name);
                    left = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter pile size 2: ");
                    right = Convert.ToInt32(Console.ReadLine());
                    if (!g.ValidMove(left, right))
                    {
                        Console.WriteLine("Not a valid move, try again: ");
                    }
                }
                while (!g.ValidMove(left, right));
            }
        }

        class Play
        {
            public const bool debugging = false;

            protected List<GeneralPlayer> Players;

            // protected SplitGame g1;

            // protected SetGame Game;

            public TreeGame History;

            protected int PlayerTurn;

            public int NumPlayers = 2; // not intended to change

            public int NumMoves { get; set; }

            // Constructors
            public Play(int size) // game for two players
            {
                GeneralPlayer player1 = new InteractivePlayer();
                GeneralPlayer player2 = new InteractivePlayer();

                Players = new List<GeneralPlayer> { player1, player2 };
                PlayerTurn = 0;
                NumMoves = 0;
                // g1 = new SplitGame(size);

                // Game = new SetGame(size);
                History = new TreeGame(size); // using Game is different!
                // N.B. no referential transparency!!
            }

            public Play(int size, int auto)
            {
                if (auto == 1) // game for one player against computer
                {
                    GeneralPlayer player1 = new InteractivePlayer();
                    GeneralPlayer player2 = new CleverPlayer("Computer");
                    Players = new List<GeneralPlayer> { player1, player2 };
                    PlayerTurn = 0;
                    // g1 = new SplitGame(size);

                    // Game = new SetGame(size);
                    History = new TreeGame(size);
                }
                else // auto == 2 // game for two computers
                {
                    GeneralPlayer player1 = new CleverPlayer("Computer1");
                    GeneralPlayer player2 = new CleverPlayer("Computer2");
                    Players = new List<GeneralPlayer> { player1, player2 };
                    PlayerTurn = 0;
                    // g1 = new SplitGame(size);

                    // Game = new SetGame(size);
                    History = new TreeGame(size);
                }
            }

            public GeneralPlayer LastPlayer()
            {
                return Players[(PlayerTurn - 1 + NumPlayers) % NumPlayers];
                // we do not want a negative index so ensure that does not happen
            }

            public GeneralPlayer CurrentPlayer()
            {
                return Players[PlayerTurn];
            }

            public GeneralPlayer NextPlayer()
            {
                return Players[(PlayerTurn + 1) % NumPlayers];
            }

            public bool Move()
            {
                Players[PlayerTurn].MkPair(History.GetCurrentGame(),
                    out int left, out int right);
                if (ValidMove(left, right))
                {
                    Console.WriteLine("Player {0}: {1} moves ({2}, {3})",
                        PlayerTurn + 1, Players[PlayerTurn].GetName(), left, right);
                    // Game.Move(left, right); // redundant once History working
                    History.Move(left, right);
                    PlayerTurn = (PlayerTurn + 1) % NumPlayers;
                    NumMoves++;
                    return true;
                }
                else
                {
                    Console.WriteLine("Not a valid move!");
                    return false;
                }
            }

            public void Move(int left, int right) // no longer needed but kept updated
            {
                if (ValidMove(left, right))
                {
                    Console.WriteLine("Player {0}: {1} moves ({2}, {3})",
                        PlayerTurn + 1, Players[PlayerTurn].GetName(), left, right);

                    History.GetCurrentGame().Move(left, right);
                    PlayerTurn = (PlayerTurn + 1) % NumPlayers;
                    NumMoves++;
                }
                else Console.WriteLine("Not a valid move: try again!");
            }

            public bool PlayOver()
            { return History.GetCurrentGame().GameOver(); } // need to use history now!

            public bool ValidMove(int left, int right)
            { return History.GetCurrentGame().ValidMove(left, right); } // similarly

            public int Turn()
            { return PlayerTurn + 1; }

            public void Show()
            {
/*                Underline("State of game:");
                Console.WriteLine(Game.Show());
                Console.WriteLine();
 */
                Underline("State of history:");
                History.Show2(); // whole history!
                // inconsistent naming for Show2() & other Show()s
                Console.WriteLine();
            }
        }

        // 1st attempt at a Play class with only interactive players
        class Play1
        {
            public List<string> Players { get; set; }
            public string Player2Name { get; set; }

            private SplitGame Game;

            private int PlayerTurn;

            public Play1(string p1, string p2, int size)
            {
                Players = new List<string> { p1, p2 };
                PlayerTurn = 0;
                Game = new SplitGame(size);
            }

            public void Move(int left, int right)
            {
                Console.WriteLine("Player {0}: {1} moves ({2}, {3})", 
                    PlayerTurn+1, Players[PlayerTurn], left, right);
                if (Game.ValidMove(left, right))
                {
                    Game.Move(left, right);
                    PlayerTurn = (PlayerTurn + 1) % 2;
                }
                else Console.WriteLine("Not a valid move: try again!");
            }
            public bool PlayOver()
            { return Game.GameOver(); }

            public bool ValidMove(int left, int right)
            { return Game.ValidMove(left, right); }

            public int Turn()
            { return PlayerTurn + 1; }

            public void Show()
            {
                Console.WriteLine("State of game");
                Console.WriteLine(Game.Show());
            }
        }

        static class TestBin
        {
            public static void Run()
            {
                BinTree<int> l = new BinTree<int>(2);
                var r = new BinTree<int>(5);

                var tree1 = new BinTree<int>(l, 3, r);
                Console.WriteLine("1st tree: Pre-order traversal");
                tree1.PreOrder();
                Console.WriteLine("In-order traversal");
                tree1.InOrder();
                Console.WriteLine("Post-order traversal");
                tree1.PostOrder();

                var tree2 = new BinTree<int>(null, 1, tree1);
                Console.WriteLine("2nd tree: Pre-order traversal");
                tree2.PreOrder();
                Console.WriteLine("In-order traversal");
                tree2.InOrder();
                Console.WriteLine("Post-order traversal");
                tree2.PostOrder();

                // make some abbreviations
                Func<int, BinTree<int>> leaf = BinTree<int>.Leaf;
                Func<BinTree<int>, int, BinTree<int>, BinTree<int>> node = BinTree<int>.Node;

                var tree3 = node(node(leaf(1), 2, leaf(3)), 4, node(leaf(5), 6, leaf(7)));
                Console.WriteLine("3rd tree: Pre-order traversal");
                tree3.PreOrder();
                Console.WriteLine("In-order traversal");
                tree3.InOrder();
                Console.WriteLine("Post-order traversal");
                tree3.PostOrder();
            }
        }

        static class Test // This tests the game class
        {
            public static void Run1a()
            {
                var game1 = new SplitGame(7);
                Console.WriteLine("Initially game1 is " + game1.Show());
                game1.Move(3, 4);
                Console.WriteLine("Now game1 is " + game1.Show());
                game1.Move(3, 1);
                Console.WriteLine("Now game1 is " + game1.Show());
                game1.Move(2, 1);
                Console.WriteLine("Now game1 is " + game1.Show());
                game1.Move(2, 1);
                Console.WriteLine("Now game1 is " + game1.Show());
                // bad move
                game1.Move(2, 1);
                Console.WriteLine("Now game1 is " + game1.Show());
                if (game1.GameOver())
                    Console.WriteLine("Game over!");
            }

            public static void Run1b()
            {
                var game1 = new SetGame(7);
                Console.WriteLine("Initially game1 is \n" + game1.Show());
                game1.Move(3, 4);
                Console.WriteLine("Now game1 is \n" + game1.Show());
                game1.Move(3, 1);
                Console.WriteLine("Now game1 is \n" + game1.Show());
                game1.Move(2, 1);
                Console.WriteLine("Now game1 is \n" + game1.Show());
                game1.Move(2, 1);
                Console.WriteLine("Now game1 is \n" + game1.Show());
                // bad move
                game1.Move(2, 1);
                Console.WriteLine("Now game1 is \n" + game1.Show());
                if (game1.GameOver())
                    Console.WriteLine("Game over!");
            }

            public static void Run2()
            {
                int size;
                size = GetGoodSize("First game::");
                var gamePlay = new Play(size);
                do
                {
                    bool goodMove;
                    do
                    {
                        goodMove = gamePlay.Move();
                        // (only) if the move succeeds, Move returns true
                    }
                    while (!goodMove);
                    gamePlay.Show();
                }
                while (!gamePlay.PlayOver());
                Console.WriteLine("Play is finished: winner is {0}!",
                    gamePlay.LastPlayer().GetName()); // winner is the last to play
                Console.WriteLine("{0} moves were made", gamePlay.NumMoves);

                // experimental history navigation
                Console.WriteLine("Go up!");
                gamePlay.History.Up();
                Console.WriteLine(gamePlay.History.Show());
                Console.WriteLine("Go up!");
                gamePlay.History.Up();
                Console.WriteLine(gamePlay.History.Show());
                // experimental

                size = GetGoodSize("Next game, just one player::");
                gamePlay = new Play(size, 1 /* 1 computer */);
                do
                {
                    bool goodMove;
                    do
                    {
                        goodMove = gamePlay.Move();
                    }
                    while (!goodMove);
                    gamePlay.Show();
                }
                while (!gamePlay.PlayOver());
                Console.WriteLine("New play is finished: winner is {0}!",
                    gamePlay.LastPlayer().GetName());
                Console.WriteLine("{0} moves were made", gamePlay.NumMoves);

                size = GetGoodSize("Final game");
                gamePlay = new Play(size, 2 /* 2 computers */);
                do
                {
                    bool goodMove;
                    do
                    {
                        goodMove = gamePlay.Move();
                    }
                    while (!goodMove);
                    gamePlay.Show();
                }
                while (!gamePlay.PlayOver());
                Console.WriteLine("Last play is finished: winner is {0}!",
                    gamePlay.LastPlayer().GetName());
                Console.WriteLine("{0} moves were made", gamePlay.NumMoves);
            }
        }

        public static int GetGoodSize(string message)
        {   // a classic template for getting a good value from a user
            // here we employ a user-defined Exception (see above)
            int size = 3;
            bool goodSize = false;
            Console.WriteLine(message);
            do
            {
                Console.Write("Enter size of game: ");
                try
                {
                    var userInput = Console.ReadLine();
                    size = Convert.ToInt32(userInput);
                    if (size < 3)
                    {
                        throw new SizeTooSmallException(size + " is too small a size!");
                    }
                    else // this is here for structured style
                    {
                        goodSize = true;
                    }
                }
                catch (Exception badInput) // this handles two types of error
                {
                    Console.WriteLine(badInput.Message);
                }
            } while (!goodSize);
            return size;
        }

        static void Main(string[] args)
        {
            // Test.Run1a();
            // Test.Run1b();

            // TestBin.Run();
            Test.Run2();

        }
    }
}
