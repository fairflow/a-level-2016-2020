﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Games
{
    class Playtime
    {
        public class SizeTooSmallException : Exception
        {
            public SizeTooSmallException(string message)
                : base(message)
            { }
        }
        public static void Underline(string message)
        {
            Console.WriteLine(message);
            string output = "";
            for (var i = 0; i < message.Length; i++)
            { output += "="; }
            Console.WriteLine(output);
        }

        // make an abstract GeneralGame class? Why then is a move a pair?

        public class NewGame
        {
            private int state;
            public NewGame(int size)
            {
                state = size;
            }

            public bool ValidMove(int left, int right)
            {
                var lump = left + right;
                return (left > 0 && right > 0 && left != right && state == lump);
            }

            public void MoveLeft(int left, int right) // may not be needed
            {
                if (ValidMove(left, right))
                {
                    state = left;
                }
                else
                {
                    Console.WriteLine("Illegal move, sorry.");
                }
            }

            public void MoveRight(int left, int right)
            {
                if (ValidMove(left, right))
                {
                    state = right;
                }
                else
                {
                    Console.WriteLine("Illegal move, sorry.");
                }
            }

            public bool GameOver()
            {
                return (state <= 2);
            }

            public string Show()
            {
                return state.ToString();
            }
        }

        public class SplitGame
        {
            private List<int> state;

            public SplitGame(int size)
            {
                state = new List<int> { size };
            }

            public bool ValidMove(int left, int right)
            {
                var lump = left + right;
                return (left > 0 && right > 0 && left != right && state.Contains(lump));
            }

            public void Move(int left, int right)
            {
                var lump = left + right;
                if (ValidMove(left, right))
                {
                    state.Remove(lump);
                    state.Add(left);
                    state.Add(right);
                    state.Sort((x, y) => y.CompareTo(x)); // inefficient but simple to code
                    // Sort in descending order
                }
                else
                {
                    Console.WriteLine("Illegal move, sorry.");
                }
            }

            public List<int> GetState()
            { return state; }

            public string Show()
            {
                string toShow = "";
                foreach (var lump in state)
                {
                    toShow += lump.ToString();
                    toShow += " "; // add spaces between lumps
                }
                return toShow;
            }

            public bool GameOver()
            {
                return (!state.Exists(x => x > 2));
            }
        }

        public abstract class GeneralPlayer
        {
            protected string Name;

            public GeneralPlayer()
            { // no body provided here but a constructor appears to be necessary
            }

            public GeneralPlayer(string myName)
            { Name = myName; }

            public string GetName()
            { return Name; }

            public abstract void NewPair(SplitGame g, out int left, out int right);
        }

        public class CleverPlayer : GeneralPlayer
        {
            public CleverPlayer(string n)
            { Name = n; }

            public override void NewPair(SplitGame g, out int left, out int right)
            {
                var cleverMoves = g.GetState(); // some move should be possible
                // cleverMoves.Sort((x, y) => (x > y) ? x : y); // sort in decreasing order??
                // the first test is redundant as a possible move means a lump bigger than 2
                var goodMoves = cleverMoves.FindAll(i => (i > 2 && ((i % 3) == 0 || (i % 3) == 2)));
                if (goodMoves.Count > 0)
                {
                    var best = goodMoves.Max();
                    if ((best % 3) == 2)
                    {
                        left = best - 1;
                        right = best - left; // i.e. 1 !
                    }
                    else
                    {
                        left = best - 2;
                        right = best - left; // i.e. 2 !
                    }
                }
                else
                {
                    var betterMoves = cleverMoves.FindAll(i => (i > 7 && (i % 6) == 1));
                    if (betterMoves.Count > 0)
                    {
                        var best = betterMoves.Max();
                        int n = best / 6;
                        left = 3 * n + 2;
                        right = best - left;
                    }
                    else
                    {
                        var guess = cleverMoves.Max();
                        left = (guess == 4) ? 3 : guess / 2 + 1;
                        right = guess - left;
                    }
                }
            }
        }

        public class InteractivePlayer : GeneralPlayer
        {
            public InteractivePlayer()
            {
                Console.WriteLine("Enter your name: ");
                Name = Console.ReadLine();
            }

            public override void NewPair(SplitGame g, out int left, out int right)
            {
                do
                {
                    Console.WriteLine("{0}, enter pile size 1: ", Name);
                    left = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Enter pile size 2: ");
                    right = Convert.ToInt32(Console.ReadLine());
                    if (!g.ValidMove(left, right))
                    {
                        Console.WriteLine("Not a valid move, try again: ");
                    }
                }
                while (!g.ValidMove(left, right));
            }
        }

        class Play
        {
            protected List<GeneralPlayer> Players;

            protected SplitGame Game;

            protected int PlayerTurn;

            public int NumPlayers = 2; // not intended to change

            public int NumMoves { get; set; }

            // Constructors
            public Play(int size) // game for two players
            {
                GeneralPlayer player1 = new InteractivePlayer();
                GeneralPlayer player2 = new InteractivePlayer();

                Players = new List<GeneralPlayer> { player1, player2 };
                PlayerTurn = 0;
                NumMoves = 0;
                Game = new SplitGame(size);
            }

            public Play(int size, int auto)
            {
                if (auto == 1) // game for one player against computer
                {
                    GeneralPlayer player1 = new InteractivePlayer();
                    GeneralPlayer player2 = new CleverPlayer("Computer");
                    Players = new List<GeneralPlayer> { player1, player2 };
                    PlayerTurn = 0;
                    Game = new SplitGame(size);
                }
                else // auto == 2 // game for two computers
                {
                    GeneralPlayer player1 = new CleverPlayer("Computer1");
                    GeneralPlayer player2 = new CleverPlayer("Computer2");
                    Players = new List<GeneralPlayer> { player1, player2 };
                    PlayerTurn = 0;
                    Game = new SplitGame(size);
                }
            }

            public GeneralPlayer LastPlayer()
            {
                return Players[(PlayerTurn - 1 + NumPlayers) % NumPlayers];
                // we do not want a negative index so ensure that does not happen
            }

            public GeneralPlayer CurrentPlayer()
            {
                return Players[PlayerTurn];
            }

            public GeneralPlayer NextPlayer()
            {
                return Players[(PlayerTurn + 1) % NumPlayers];
            }

            public bool Move()
            {
                Players[PlayerTurn].NewPair(Game, out int left, out int right);
                if (ValidMove(left, right))
                {
                    Console.WriteLine("Player {0}: {1} moves ({2}, {3})",
                        PlayerTurn + 1, Players[PlayerTurn].GetName(), left, right);
                    Game.Move(left, right);
                    PlayerTurn = (PlayerTurn + 1) % NumPlayers;
                    NumMoves++;
                    return true;
                }
                else
                {
                    Console.WriteLine("Not a valid move!");
                    return false;
                }
            }

            public void Move(int left, int right) // no longer needed
            {
                if (ValidMove(left, right))
                {
                    Console.WriteLine("Player {0}: {1} moves ({2}, {3})",
                        PlayerTurn + 1, Players[PlayerTurn].GetName(), left, right);

                    Game.Move(left, right);
                    PlayerTurn = (PlayerTurn + 1) % NumPlayers;
                    NumMoves++;
                }
                else Console.WriteLine("Not a valid move: try again!");
            }

            public bool PlayOver()
            { return Game.GameOver(); }

            public bool ValidMove(int left, int right)
            { return Game.ValidMove(left, right); }

            public int Turn()
            { return PlayerTurn + 1; }

            public void Show()
            {
                Underline("State of game:");
                Console.WriteLine(Game.Show());
                Console.WriteLine();
            }
        }

        class NewPlay
        {
            protected List<GeneralPlayer> Players;

            protected NewGame Game;

            protected int PlayerTurn;

            public int NumPlayers = 2; // not intended to change

            public int NumMoves { get; set; }

            // Constructors
            public NewPlay(int size) // game for two players
            {
                GeneralPlayer player1 = new InteractivePlayer();
                GeneralPlayer player2 = new InteractivePlayer();

                Players = new List<GeneralPlayer> { player1, player2 };
                PlayerTurn = 0;
                NumMoves = 0;
                Game = new NewGame(size);
            }

            public NewPlay(int size, int auto)
            {
                if (auto == 1) // game for one player against computer
                {
                    GeneralPlayer player1 = new InteractivePlayer();
                    GeneralPlayer player2 = new CleverPlayer("Computer");
                    Players = new List<GeneralPlayer> { player1, player2 };
                    PlayerTurn = 0;
                    Game = new NewGame(size);
                }
                else // auto == 2 // game for two computers
                {
                    GeneralPlayer player1 = new CleverPlayer("Computer1");
                    GeneralPlayer player2 = new CleverPlayer("Computer2");
                    Players = new List<GeneralPlayer> { player1, player2 };
                    PlayerTurn = 0;
                    Game = new NewGame(size);
                }
            }

            public GeneralPlayer LastPlayer()
            {
                return Players[(PlayerTurn - 1 + NumPlayers) % NumPlayers];
                // we do not want a negative index so ensure that does not happen
            }

            public GeneralPlayer CurrentPlayer()
            {
                return Players[PlayerTurn];
            }

            public GeneralPlayer NextPlayer()
            {
                return Players[(PlayerTurn + 1) % NumPlayers];
            }

            public bool Move()
            {
                Players[PlayerTurn].NewPair(Game, out int left, out int right);
                if (ValidMove(left, right))
                {
                    Console.WriteLine("Player {0}: {1} moves ({2}, {3})",
                        PlayerTurn + 1, Players[PlayerTurn].GetName(), left, right);
                    Game.Move(left, right);
                    PlayerTurn = (PlayerTurn + 1) % NumPlayers;
                    NumMoves++;
                    return true;
                }
                else
                {
                    Console.WriteLine("Not a valid move!");
                    return false;
                }
            }

            public void Move(int left, int right) // no longer needed
            {
                if (ValidMove(left, right))
                {
                    Console.WriteLine("Player {0}: {1} moves ({2}, {3})",
                        PlayerTurn + 1, Players[PlayerTurn].GetName(), left, right);

                    Game.Move(left, right);
                    PlayerTurn = (PlayerTurn + 1) % NumPlayers;
                    NumMoves++;
                }
                else Console.WriteLine("Not a valid move: try again!");
            }

            public bool PlayOver()
            { return Game.GameOver(); }

            public bool ValidMove(int left, int right)
            { return Game.ValidMove(left, right); }

            public int Turn()
            { return PlayerTurn + 1; }

            public void Show()
            {
                Underline("State of game:");
                Console.WriteLine(Game.Show());
                Console.WriteLine();
            }
        }

        static class Test // This just tests the game class
        {
            public static void Run()
            {
                var game1 = new SplitGame(7);
                Console.WriteLine("Initially game1 is " + game1.Show());
                game1.Move(3, 4);
                Console.WriteLine("Now game1 is " + game1.Show());
                game1.Move(3, 1);
                Console.WriteLine("Now game1 is " + game1.Show());
                game1.Move(2, 1);
                Console.WriteLine("Now game1 is " + game1.Show());
                game1.Move(2, 1);
                Console.WriteLine("Now game1 is " + game1.Show());
                // bad move
                game1.Move(2, 1);
                Console.WriteLine("Now game1 is " + game1.Show());
                if (game1.GameOver())
                    Console.WriteLine("Game over!");
            }
        }

        public static int GetGoodSize(string message)
        {   // a classic template for getting a good value from a user
            // here we employ a user-defined Exception (see above)
            int size = 3;
            bool goodSize = false;
            Console.WriteLine(message);
            do
            {
                Console.Write("Enter size of game: ");
                try
                {
                    var userInput = Console.ReadLine();
                    size = Convert.ToInt32(userInput);
                    if (size < 3)
                    {
                        throw new SizeTooSmallException(size + " is too small a size!");
                    }
                    else // this is here for structured style
                    {
                        goodSize = true;
                    }
                }
                catch (Exception badInput) // this handles two types of error
                {
                    Console.WriteLine(badInput.Message);
                }
            } while (!goodSize);
            return size;
        }

        public static void DisplayMenu()
        {
            Console.WriteLine("\n*********************************");
            Console.WriteLine("**********    MENU     **********");
            Console.WriteLine("*********************************");
            Console.WriteLine("0. 2 player game");
            Console.WriteLine("1. 1 player game with computer opponent");
            Console.WriteLine("2. 0 player game with 2 computers");

            Console.WriteLine("Q. Quit");
            Console.Write("\n Enter your choice: ");
        }

        public static void Run()
        {
            string choice = "";
            int size;
            while (choice != "Q")
            {
                DisplayMenu();
                choice = Console.ReadLine();
                switch (choice)
                {
                    case "0":
                        size = GetGoodSize("2 player game");
                        var gamePlay = new Play(size);
                        do
                        {
                            bool goodMove;
                            do
                            {
                                goodMove = gamePlay.Move();
                                // (only) if the move succeeds, Move returns true
                            }
                            while (!goodMove);
                            gamePlay.Show();
                        }
                        while (!gamePlay.PlayOver());
                        Console.WriteLine("Play is finished: winner is {0}!",
                            gamePlay.LastPlayer().GetName()); // winner is the last to play
                        Console.WriteLine("{0} moves were made", gamePlay.NumMoves);
                        break;

                    case "1":
                        size = GetGoodSize("1 player game");
                        gamePlay = new Play(size, 1 /* 1 computer */);
                        do
                        {
                            bool goodMove;
                            do
                            {
                                goodMove = gamePlay.Move();
                            }
                            while (!goodMove);
                            gamePlay.Show();
                        }
                        while (!gamePlay.PlayOver());
                        Console.WriteLine("Play is finished: winner is {0}!",
                            gamePlay.LastPlayer().GetName());
                        Console.WriteLine("{0} moves were made", gamePlay.NumMoves);
                        break;

                    case "2":
                        size = GetGoodSize("0 player game");
                        gamePlay = new Play(size, 2 /* 2 computers */);
                        do
                        {
                            bool goodMove;
                            do
                            {
                                goodMove = gamePlay.Move();
                            }
                            while (!goodMove);
                            gamePlay.Show();
                        }
                        while (!gamePlay.PlayOver());
                        Console.WriteLine("Play is finished: winner is {0}!",
                            gamePlay.LastPlayer().GetName());
                        Console.WriteLine("{0} moves were made", gamePlay.NumMoves);
                        break;

                    case "Q":
                        Console.WriteLine("Playtime over, press Enter to close.");
                        Console.ReadLine();
                        break;
                }
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Running SplitGame");
            Test.Run(); // preliminary testing
            Run(); // main action
        }
    }
}
