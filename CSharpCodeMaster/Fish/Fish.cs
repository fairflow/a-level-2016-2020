﻿using System;

namespace Fish
{
    class FishByRef
    { // provides a _static_ method acting as a standalone procedure
        public static void feed(ref string state, ref int size)
        {
            size++;
            Console.WriteLine("Fish fed");
            if (size == 5) { state = "FISH"; }
        }
    }

    class Animal
    {
        private string state;
        private int size;

        // Constructor(s) use the name of the class
        public Animal(string s, int n) // name parameters differently
        {
            state = s; // you can write this.state and this.size too
            size = n;
        }

        public void feed() // Every Animal carries its own feeding method
        {
            Console.WriteLine("Animal {0} fed", state);
            size++;
            if (size >= 5) { state = "FISH"; }
            // Ex: make this more general by turning any Animal state to upper case
        }

        public string getState()
        { return state; }

        public int getSize()
        { return size; }
        // NOTE: there are quicker ways of defining the same functions
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Procedural fishing begun...\n");
            string fishState = "fish";
            int fishSize = 1;
            Console.WriteLine("To start with we have a {0}", fishState);
            while (fishState != "FISH")
            { FishByRef.feed(ref fishState, ref fishSize); }
            Console.WriteLine("It is now a big {0}", fishState);

            Console.WriteLine("\nFinished fishing!\n");

            Console.WriteLine("Classy animaling begun...\n");
            var myAnimal = new Animal("fish", 3); // it is obvious which type is meant
            Console.WriteLine("To start with we have another {0}", myAnimal.getState());
            while (myAnimal.getState() != "FISH")
            { myAnimal.feed(); } // Ex: why are no parameters needed here?
            Console.WriteLine("Animal is now a big {0}", myAnimal.getState());

            Console.WriteLine("\nFinished animaling!\n");
        }
    }
}
