﻿using System;
using System.Collections.Generic;

namespace DynamicStack
{
    public class Stack<Type>
    {
        private List<Type> Data; // where we store the stacked elements
        private int Size;        // the current size of the stack
        private int MaxSize;     // the maximum size of the stack

        public Stack() // Construct an empty stack with default limit
        {
            Data = new List<Type>();
            Size = 0;
            MaxSize = 1000000; // a million elements should do for now
        }

        public Stack(int max) // Construct an empty stack with given limit
        {
            Data = new List<Type>();
            Size = 0;
            MaxSize = max;
        }

        public Stack(int max, Type [] starter) // Start a stack from existing array
        {
            Data = new List<Type>(starter); // luckily List has just the right constructor
            int newLen = starter.GetLength(0);
            Size = newLen;
            MaxSize = max;
        }

        public void Push(Type element)
        {
            if (!Full())
            {
                Size++;
                Data.Add(element);
                return;
            }
            Console.WriteLine("Stack full ({0} items).  Pop some items and try again.", Size);
        }

        public Type Pop()
        {
            if (!Empty())
            {
                Size--;
                var top = Data[0];
                Data.RemoveAt(0);
                return top;       // notice the order: save top, remove it, return
            }
            Console.WriteLine("Stack empty.  Push something and try again.");
            return default;
        }

        public bool Empty()
        {
            return (Size <= 0);
        }

        public bool Full()
        {
            return (Size >= MaxSize);
        }

        public bool Extend(int increase)
        {
            if (increase > 0)
            {
                MaxSize += increase;
                return true;
            }
            Console.WriteLine("Try to extend by a positive quantity next time!");
            return false;
        }

        public List<Type> GetItems()
        {
            return Data;
        }
    }
}
