﻿using System;

namespace DynamicStack
{
    class Program
    {
        static void checkEmpty(Stack<string> st)
        {
            if (st.Empty())
            {
                Console.WriteLine("Stack is empty");
            }
            else
            if (st.Full())
            {
                Console.WriteLine("Stack is full");
            }
            else
            {
                Console.WriteLine("Stack is just fine");
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Demonstrating dynamic stack datatype\n");

            Stack<string> animals = new Stack<string>(5);

            animals.Push("manatee");
            animals.Push("pig");
            animals.Push("chicken");
            animals.Push("fruit-fly");
            animals.Push("alligator");

            Console.WriteLine("Get all items and write out:");
            foreach (string animal in animals.GetItems())
            {
                Console.WriteLine(animal);
            }

            Console.WriteLine("\nTesting Pop\n");
            Console.WriteLine("Popping '{0}'", animals.Pop());
            Console.WriteLine("Popping '{0}'", animals.Pop());

            checkEmpty(animals);

            animals.Push("pigeon");
            animals.Push("diplodocus");
            Console.WriteLine("\nInspect stack again:");
            foreach (string animal in animals.GetItems())
            {
                Console.WriteLine(animal);
            }

            checkEmpty(animals);

            Console.WriteLine("\nNow pop five items...");
            animals.Pop();
            animals.Pop();
            animals.Pop();
            animals.Pop();
            animals.Pop();

            checkEmpty(animals);

            Console.WriteLine("\nTesting creating a stack from an array:");
            Stack<string> characters =
                new Stack<string>(7, new string[] { "a", "b", "c", "d", "e", "f" });
            foreach (string c in characters.GetItems())
            {
                Console.WriteLine(c);
            }

        }
    }
}
