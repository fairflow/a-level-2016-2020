﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FractionalBanking
{
    class Bank  // banks will have households as domestic customers and restaurants as business customers
    {
        public class Transaction
        {
            public decimal Amount { get; }
            public DateTime Date { get; }
            public string Notes { get; }

            public Account DebitAccount { get; }   // the account to debit
            public Account CreditAccount { get; }  // the account to credit

            public Transaction(decimal amount, DateTime date, string notes, Account debit, Account credit)
            {
                Amount = amount;
                Date = date;
                Notes = notes;
                DebitAccount = debit;
                CreditAccount = credit;
            }

            public void Post()
            {
                if (DebitAccount.NormalBalance == "debit")
                {
                    DebitAccount.Balance += Amount;
                }
                else
                if (DebitAccount.NormalBalance == "credit")
                {
                    DebitAccount.Balance -= Amount;
                }
                if (CreditAccount.NormalBalance == "credit")
                {
                    CreditAccount.Balance += Amount;
                }
                else
                if (CreditAccount.NormalBalance == "debit")
                {
                    CreditAccount.Balance -= Amount;
                }
            }
        }

        public class Account
        {
            public int Number { get; }
            public string Name { get; }
            public string NormalBalance { get; } // i.e. "debit" or "credit" // better use an enum

            public decimal Balance { get; set; } // ok not very secure
            // no nested accounts needed for simple simulation
            public Account(int number, string name, string type, decimal balance)
                // create an account with opening balance (which CANNOT be negative)
            {
                Number = number;
                Name = name;
                NormalBalance = type;

                Balance = balance; // poor naming style but whatever
            }

            public Account()
            {
            }

            public string GetDetails()
            {
                string details;
                details = NormalBalance + " account: " + Name  + "a/c no. " +
                    Number.ToString() + " balance: £" + Balance.ToString();
                return details;
            }
        }

        //  
        private static Random rnd = new Random();
        protected decimal capital;
        protected static int nextAccountNumber = 100001;
        protected string sortCode;

        protected Account loanAccount;
        
        protected List<(Account a, Household h)> domesticCustomers = new List<(Account, Household)>();
        // the int is the account number of the household
        protected List<(Account account, Company c)> businessCustomers = new List<(Account, Company)>();
        // the int is the account number of the company
        // a dictionary might be a better choice but indexing on Account?  Only how to search?
        // 1NF considerations now arise.  Database theory has something to recommend it!
        protected List<Transaction> transactions = new List<Transaction>();

        public Bank(string sort, decimal startCapital)
        {
            capital = startCapital;
            sortCode = sort;
            loanAccount = new Account(nextAccountNumber++, "Loan account " + sortCode, "debit", 0.0M);
            // need to think about the starting balance
        }

        public Account AddDomestic(Household h) // starting account with 0 balance
        {
            var hAccount = new Account(nextAccountNumber++, h.Name + " account", "credit" /* ?? */, 0.0M);
            domesticCustomers.Add((hAccount, h));
            return hAccount;
        }

        public Account AddDomestic(Household h, decimal initialBalance) // starting account with an initial balance
        {
            var hAccount = new Account(nextAccountNumber++, h.Name + " account", "credit" /* ?? */, 0.0M);
            domesticCustomers.Add((hAccount, h));
            var t = new Transaction(initialBalance, DateTime.Today, "start balance", loanAccount, hAccount);
            transactions.Add(t);
            t.Post(); // not Post(t) ! // we may not want to do this immediately but...
            return hAccount;
        }

        public bool ApproveBusinessApplication(Company c, decimal loanAmount)
        {
            if (rnd.Next(0, 2) == 0)
            { return loanAmount < 1000000.01M;
            }
            else
            { return false;
            }
        }

        public Account OpenBusinessAccount(Company c, decimal loanAmount)
        {
            if (ApproveBusinessApplication(c, loanAmount))
            {
                var bAccount = new Account(nextAccountNumber++, c.name + " account", "credit" /* ?? */, 0.0M);
                businessCustomers.Add((bAccount, c));
                var t = new Transaction(loanAmount, DateTime.Today, "start balance", loanAccount, bAccount);
                transactions.Add(t);
                t.Post(); // not Post(t) ! // we may not want to do this immediately but...
                return bAccount;
            } // this is messed up since we need to allocate a separate account to the requesting business: or do we?
            // can we simply get them to share the account, get the company to post transactions and let them manage their own account?
            else
            {
                return null; // this should not happen
            }
        }

    }

    class Household
    {
        private static Random rnd = new Random();
        public string Name { get; set; } // protection levels!
        protected decimal chanceEatOutPerDay;
        protected int xCoord, yCoord, ID;
        protected static int nextID = 1;

        public Household(int x, int y)
        {
            Name = "house #" + nextID.ToString(); // until a better idea comes along
            xCoord = x;
            yCoord = y;
            chanceEatOutPerDay = (decimal) rnd.NextDouble();
            ID = nextID;
            nextID++;
        }

        public string GetDetails()
        {
            string details;
            details = ID.ToString() + "     Coordinates: (" + xCoord.ToString() + ", " + yCoord.ToString() + ")     Eat out probability: " + chanceEatOutPerDay.ToString();
            return details;
        }

        public decimal GetChanceEatOut()
        {
            return chanceEatOutPerDay;
        }

        public int GetX()
        {
            return xCoord;
        }

        public int GetY()
        {
            return yCoord;
        }
    }

    class Settlement
    {
        private static Random rnd = new Random();
        protected int startNoOfHouseholds, xSize, ySize;
        protected List<Household> households = new List<Household>();

        public Settlement(int xSizeStarter, int ySizeStarter, int householdStarter)
        {
            xSize = xSizeStarter;
            ySize = ySizeStarter;
            startNoOfHouseholds = householdStarter;
            CreateHouseholds();
        }

        public void GetWeightedCentre(ref decimal xM, ref decimal yM)
        {
            xM = 0;
            yM = 0;
            decimal expectation = 0;
            foreach (var h in households)
            {
                xM += h.GetX() * h.GetChanceEatOut();
                yM += h.GetY() * h.GetChanceEatOut();
                expectation += h.GetChanceEatOut();
            }
            xM /= expectation;
            yM /= expectation;
            Console.WriteLine("Centre of mass has coordinates ({0}, {1})", xM, yM);
        }

        public int GetNumberOfHouseholds()
        {
            return households.Count;
        }

        public int GetXSize()
        {
            return xSize;
        }

        public int GetYSize()
        {
            return ySize;
        }

        public void GetRandomLocation(ref int x, ref int y)
        {
            x = Convert.ToInt32(rnd.NextDouble() * xSize);
            y = Convert.ToInt32(rnd.NextDouble() * ySize);
        }

        public void CreateHouseholds()
        {
            for (int count = 0; count < startNoOfHouseholds; count++)
            {
                AddHousehold();
            }
        }

        public void AddHousehold()
        {
            int x = 0, y = 0;
            GetRandomLocation(ref x, ref y);
            Household temp = new Household(x, y);
            households.Add(temp);
        }

        public void DisplayHouseholds()
        {
            Console.WriteLine("\n**********************************");
            Console.WriteLine("*** Details of all households: ***");
            Console.WriteLine("**********************************\n");
            foreach (var h in households)
            {
                Console.WriteLine(h.GetDetails());
            }
            Console.WriteLine();
            decimal xM = 0;
            decimal yM = 0;
            GetWeightedCentre(ref xM, ref yM);
        }

        public void DisplayHouseholdGrid()
        {
            Console.WriteLine("\n**********************************");
            Console.WriteLine("*** Grid of all households:    ***");
            Console.WriteLine("**********************************\n");
            const int blockSize = 20;

            /*
             *        foreach (var h in households)
                      {
                          Console.WriteLine(h.GetDetails());
                      }
                      */

            // for now print out details to compare with grid output
            int[,] houseCount = new int[xSize / blockSize, ySize / blockSize];
            // initialise house count
            for (int i = 0; i < houseCount.GetLength(0); i++)
            {
                for (int j = 0; j < houseCount.GetLength(1); j++)
                {
                    houseCount[i, j] = 0;
                    //Console.Write(0);
                }
                //Console.WriteLine();
            }
            // loop through each household adding 
            foreach (var h in households)
            {
                houseCount[(h.GetX() - 1) / blockSize, (h.GetY() - 1) / blockSize]++;
                // -1 in each case to deal with situation where a coordinate is zSize (z = x or y)
            }
            char[,] grid = new char[xSize / blockSize, ySize / blockSize];
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    if (houseCount[i, j] > 0)
                    {
                        if (houseCount[i, j] < 10)
                        {
                            grid[i, j] = houseCount[i, j].ToString()[0];
                            // a string representing a number between 1 and 9 has only a single character
                        }
                        else
                        {
                            grid[i, j] = '*';
                        }
                    }
                    else
                    {
                        grid[i, j] = ' '; // every unoccupied grid point is blank
                    }
                }
            }
            // display grid
            for (int i = 0; i < grid.GetLength(0); i++)
            {
                for (int j = 0; j < grid.GetLength(1); j++)
                {
                    Console.Write(grid[i, j]);
                    Console.Write(' ');
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }


        public bool FindOutIfHouseholdEatsOut(int householdNo, ref int x, ref int y)
        {
            decimal eatOutRNo = (decimal) rnd.NextDouble();
            if (eatOutRNo < households[householdNo].GetChanceEatOut())
            { // no need to reassign x and y if the household is not eating out
                x = households[householdNo].GetX();
                y = households[householdNo].GetY();
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    class LargeSettlement : Settlement
    {
        public LargeSettlement(int extraXSize, int extraYSize, int extraHouseholds)
            : base(1000, 1000, 250) // bad bad bad but effective
        {
            xSize += extraXSize;
            ySize += extraYSize;
            startNoOfHouseholds += extraHouseholds;
            for (int count = 1; count < extraHouseholds + 1; count++)
            {
                AddHousehold();
            }
        }
    }

    class CustomSettlement : Settlement
    {
        public CustomSettlement(int startXSize, int startYSize, int startHouseholds)
             : base(0, 0, 0)
        // adding a fresh parameter does not prevent base initialisation
        {
            xSize = startXSize;
            ySize = startYSize;
            startNoOfHouseholds += startHouseholds;
            for (int count = 1; count < startHouseholds + 1; count++)
            {
                AddHousehold();
            }
        }
    }

    class Outlet
    {
        private static Random rnd = new Random();
        protected int visitsToday, xCoord, yCoord, capacity, maxCapacity;
        protected decimal dailyCosts; // 0 VAT rated by assumption

        public Outlet(int xCoord, int yCoord, int maxCapacityBase)
        {
            this.xCoord = xCoord;
            this.yCoord = yCoord;
            capacity = Convert.ToInt32(maxCapacityBase * 0.6);
            maxCapacity = maxCapacityBase + Convert.ToInt32(rnd.NextDouble() * 50) - Convert.ToInt32(rnd.NextDouble() * 50);
            dailyCosts = (decimal) ((maxCapacityBase * 0.2) + capacity * 0.5 + 100);
            NewDay();
        }

        public int GetCapacity()
        {
            return capacity;
        }

        public int GetX()
        {
            return xCoord;
        }

        public int GetY()
        {
            return yCoord;
        }

        public void AlterDailyCost(decimal amount)
        {
            dailyCosts += amount;
        }

        public int AlterCapacity(int change)
        {
            int oldCapacity = capacity;
            capacity += change;
            if (capacity > maxCapacity)
            {
                capacity = maxCapacity;
                return maxCapacity - oldCapacity;
            }
            else if (capacity < 0)
            {
                capacity = 0;
            }
            dailyCosts = (decimal) (maxCapacity * 0.2 + capacity * 0.5 + 100);
            return change;
        }

        public void IncrementVisits()
        {
            visitsToday++;
        }

        public void NewDay()
        {
            visitsToday = 0;
        }

        public decimal CalculateDailyProfitLoss(decimal avgCostPerMeal, decimal avgPricePerMeal, bool vatRegistered, decimal vatRate)
        {
            var valueAddedPerMeal = vatRegistered ? (avgPricePerMeal - avgCostPerMeal) * vatRate : (avgPricePerMeal - avgCostPerMeal);
            return valueAddedPerMeal * visitsToday - dailyCosts;
        }

        public string GetDetails()
        {
            string details = "";
            details = "Coordinates: (" + xCoord.ToString() + ", " + yCoord.ToString() + ")     Capacity: " + capacity.ToString() + "      Maximum Capacity: ";
            details += maxCapacity.ToString() + "      Daily Costs: " + dailyCosts.ToString() + "      Visits today: " + visitsToday.ToString();
            return details;
        }
    }

    class Company // next, get companies to request loans and banks to approve them
    {
        private static Random rnd = new Random();
        private bool vatRegistered;
        static private decimal vatRate = 1.2M;
        public string name { get; }
        public string category { get; }
        protected decimal balance, reputationScore, avgCostPerMeal, avgPricePerMeal, dailyCosts, familyOutletCost, fastFoodOutletCost, namedChefOutletCost, fuelCostPerUnit, baseCostOfDelivery;
        protected List<Outlet> outlets = new List<Outlet>();
        protected int familyFoodOutletCapacity, fastFoodOutletCapacity, namedChefOutletCapacity;

        // accounts: bank account, VAT account, expense account, income account
        protected Bank.Account bankAC; // = new Bank.Account(name, ); // etc.

        public Company(string name, string category, decimal balance, int x, int y, decimal fuelCostPerUnit, decimal baseCostOfDelivery, Bank b)
        {
            familyOutletCost = 1000;
            fastFoodOutletCost = 2000;
            namedChefOutletCost = 15000;
            familyFoodOutletCapacity = 150;
            fastFoodOutletCapacity = 200;
            namedChefOutletCapacity = 50;
            this.name = name;
            this.category = category;
            this.balance = balance;
            this.fuelCostPerUnit = fuelCostPerUnit;
            this.baseCostOfDelivery = baseCostOfDelivery;
            reputationScore = 100;
            dailyCosts = 100;
            if (category == "fast food")
            {
                vatRegistered = true;
                avgCostPerMeal = 5;
                avgPricePerMeal = 10;
                reputationScore += (decimal) (rnd.NextDouble() * 10 - 8);
            }
            else if (category == "family")
            {
                vatRegistered = false;
                avgCostPerMeal = 12;
                avgPricePerMeal = 14;
                reputationScore += (decimal) (rnd.NextDouble() * 30 - 5);
            }
            else
            {
                vatRegistered = true;
                avgCostPerMeal = 20;
                avgPricePerMeal = 40;
                reputationScore += (decimal) (rnd.NextDouble() * 50);
            }
            if (b.ApproveBusinessApplication(this, balance)) // can we do this?  Ask another class for approval before creation?
                // oh yes we can!
            {
                bankAC = b.OpenBusinessAccount(this, balance);
                OpenOutlet(x, y); // outlets do not have their own accounts (yet)
                /* and if not?  What should happen then?
                 * Well, no outlets are opened so the company remains rather virtual; it cannot trade
                 */
            }
        }

        public string GetName()
        {
            return name;
        }

        public int GetNumberOfOutlets()
        {
            return outlets.Count;
        }

        public decimal GetReputationScore()
        {
            return reputationScore;
        }

        public void AlterDailyCosts(decimal change)
        {
            var newCost = dailyCosts + change;
            if (newCost > 0)
            {
                dailyCosts = newCost; // but now the alert message is not correct so this is not so good
                // move this test to the calling function ProcessCostChangeEvent
            }
        }
        public void AlterAvgCostPerMeal(decimal change)
        {
            avgCostPerMeal += change;
        }

        public void AlterFuelCostPerUnit(decimal change)
        {
            fuelCostPerUnit += change;
        }

        public void AlterReputation(decimal change)
        {
            reputationScore += change;
        }

        public void NewDay()
        {
            foreach (var o in outlets)
            {
                o.NewDay();
            }
        }

        public void AddVisitToNearestOutlet(int x, int y)
        {
            int nearestOutlet = 0;
            double nearestOutletDistance, currentDistance;
            nearestOutletDistance = Math.Sqrt((Math.Pow(outlets[0].GetX() - x, 2)) + (Math.Pow(outlets[0].GetY() - y, 2)));
            for (int current = 1; current < outlets.Count; current++)
            {
                currentDistance = Math.Sqrt((Math.Pow(outlets[current].GetX() - x, 2)) + (Math.Pow(outlets[current].GetY() - y, 2)));
                if (currentDistance < nearestOutletDistance)
                {
                    nearestOutletDistance = currentDistance;
                    nearestOutlet = current;
                }
            }
            outlets[nearestOutlet].IncrementVisits();
        }

        public string GetDetails() // useful to know fuel costs and travel distance
        {
            string details = "";
            details += "Name: " + name + "\nType of business: " + category + "\n";
            details += "Current balance: " + balance.ToString() + "\nAverage cost per meal: " + avgCostPerMeal.ToString() + "\n";
            details += "Average price per meal: " + avgPricePerMeal.ToString() + "\nDaily costs: " + dailyCosts.ToString() + "\n";
            details += "Fuel costs: " + fuelCostPerUnit.ToString() + "\n";
            details += "Delivery costs: " + CalculateDeliveryCost().ToString() + "\n";
            details += "\nReputation: " + reputationScore.ToString() + "\n\n";
            details += "Number of outlets: " + outlets.Count.ToString() + "\nOutlets\n";
            for (int current = 1; current < outlets.Count + 1; current++)
            {
                details += current + ". " + outlets[current - 1].GetDetails() + "\n";
            }
            return details;
        }

        public string ProcessDayEnd()
        {
            string details = "";
            decimal profitLossFromOutlets = 0;
            decimal profitLossFromThisOutlet = 0;
            decimal deliveryCosts;
            if (outlets.Count > 1)
            {
                deliveryCosts = baseCostOfDelivery + CalculateDeliveryCost();
            }
            else
            {
                deliveryCosts = baseCostOfDelivery;
            }
            details += "Daily costs for company: " + dailyCosts.ToString() + "\nCost for delivering produce to outlets: " + deliveryCosts.ToString() + "\n";
            for (int current = 0; current < outlets.Count; current++)
            {
                profitLossFromThisOutlet = outlets[current].CalculateDailyProfitLoss(avgCostPerMeal, avgPricePerMeal, vatRegistered, vatRate);
                details += "Outlet " + (current + 1) + " profit/loss: " + profitLossFromThisOutlet.ToString() + "\n";
                profitLossFromOutlets += profitLossFromThisOutlet;
            }
            details += "Previous balance for company: " + balance.ToString() + "\n";
            balance += profitLossFromOutlets - dailyCosts - deliveryCosts;
            // deal with overdraft situation
            details += "New balance for company: " + balance.ToString();
            return details;
        }

        public bool CloseOutlet(int ID)
        {
            outlets.RemoveAt(ID);
            return (outlets.Count == 0);
        }

        public void ExpandOutlet(int ID)
        {
            int change, result;
            Console.Write("Enter amount you would like to expand the capacity by: ");
            change = Convert.ToInt32(Console.ReadLine());
            result = outlets[ID].AlterCapacity(change);
            if (result == change)
            {
                Console.WriteLine("Capacity adjusted.");
            }
            else
            {
                Console.WriteLine("Only some of that capacity added, outlet now at maximum capacity.");
            }
        }

        public void OpenOutlet(int x, int y)
        {
            int capacity;
            if (category == "fast food")
            {
                balance -= fastFoodOutletCost;
                capacity = fastFoodOutletCapacity;
            }
            else if (category == "family")
            {
                balance -= familyOutletCost;
                capacity = familyFoodOutletCapacity;
            }
            else
            {
                balance -= namedChefOutletCost;
                capacity = namedChefOutletCapacity;
            }
            Outlet newOutlet = new Outlet(x, y, capacity);
            outlets.Add(newOutlet);
        }

        public List<int> GetListOfOutlets()
        {
            List<int> temp = new List<int>();
            for (int current = 0; current < outlets.Count; current++)
            {
                temp.Add(current);
            }
            return temp;
        }

        private double GetDistanceBetweenTwoOutlets(int outlet1, int outlet2)
        {
            return Math.Sqrt((Math.Pow(outlets[outlet1].GetX() - outlets[outlet2].GetX(), 2)) + (Math.Pow(outlets[outlet1].GetY() - outlets[outlet2].GetY(), 2)));
        }

        public decimal CalculateDeliveryCost()
        {
            List<int> listOfOutlets = new List<int>(GetListOfOutlets());
            double totalDistance = 0;
            decimal totalCost = 0;
            for (int current = 0; current < listOfOutlets.Count - 1; current++)
            {
                totalDistance += GetDistanceBetweenTwoOutlets(listOfOutlets[current], listOfOutlets[current + 1]);
            }
            totalCost =  ((decimal) totalDistance * (vatRegistered ? fuelCostPerUnit * vatRate : fuelCostPerUnit));
            return totalCost;
        }
    }

    class Simulation
    {
        private static Random rnd = new Random();
        protected Settlement simulationSettlement;
        protected int noOfCompanies;
        protected readonly decimal fuelCostPerUnit, baseCostForDelivery;
        protected List<Company> companies = new List<Company>();

        protected Bank simBank = new Bank("00-00-00", 20000000);

        public void AddDefaultCompanies()
        {
            noOfCompanies = 3;
            Company company1 = new Company("AQA Burgers", "fast food", 100000, 200, 203, fuelCostPerUnit, baseCostForDelivery, simBank);
            companies.Add(company1);
            companies[0].OpenOutlet(300, 987);
            companies[0].OpenOutlet(500, 500);
            companies[0].OpenOutlet(305, 303);
            companies[0].OpenOutlet(874, 456);
            companies[0].OpenOutlet(23, 408);
            companies[0].OpenOutlet(412, 318);
            Company company2 = new Company("Ben Thor Cuisine", "named chef", 100400, 390, 800, fuelCostPerUnit, baseCostForDelivery, simBank);
            companies.Add(company2);
            Company company3 = new Company("Paltry Poultry", "fast food", 25000, 800, 390, fuelCostPerUnit, baseCostForDelivery, simBank);
            companies.Add(company3);
            companies[2].OpenOutlet(400, 390);
            companies[2].OpenOutlet(820, 370);
            companies[2].OpenOutlet(800, 600);
        }

        public Simulation()
        {
            fuelCostPerUnit = 0.0098M;
            baseCostForDelivery = 100;
            string choice;
            Console.Write("Enter L for a large settlement, C for a custom-sized settlement, anything else for a normal size settlement: ");
            choice = Console.ReadLine();
            if (choice == "L")
            {
                int extraX, extraY, extraHouseholds;
                Console.Write("Enter additional amount to add to X size of settlement: ");
                extraX = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter additional amount to add to Y size of settlement: ");
                extraY = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter additional number of households to add to settlement: ");
                // extraHouseholds = Convert.ToInt32(Console.ReadLine());
                bool success;
                do
                { success = int.TryParse(Console.ReadLine(), out extraHouseholds); }
                while (!success);
                simulationSettlement = new LargeSettlement(extraX, extraY, extraHouseholds);
            }
            else
            if (choice == "C")
            {
                int extraX, extraY, extraHouseholds; // change these variable names for consistency
                Console.Write("Enter initial X size of settlement: ");
                extraX = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter initial Y size of settlement: ");
                extraY = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter number of households to start settlement with: ");
                // extraHouseholds = Convert.ToInt32(Console.ReadLine());
                bool success;
                do
                { success = int.TryParse(Console.ReadLine(), out extraHouseholds) && extraHouseholds > 0; }
                while (!success);
                simulationSettlement = new CustomSettlement(extraX, extraY, extraHouseholds);
            }
            else
            {
                simulationSettlement = new Settlement(1000, 1000, 250);
            }
            int ownCompanies;
            bool okInput;
            do
            {
                Console.Write("Enter 0 for default companies, " +
                    "\nor a positive number of your own companies to start the simulation: ");
                choice = Console.ReadLine();
                okInput = int.TryParse(choice, out ownCompanies);
            } while (!okInput);
            if (ownCompanies == 0)
            {
                AddDefaultCompanies();
            }
            else
            {
                // Console.Write("Enter number of companies that exist at start of simulation: ");
                noOfCompanies = ownCompanies;
                for (int count = 1; count < noOfCompanies + 1; count++)
                {
                    AddCompany();
                }
            }
        }

        public void DisplayMenu()
        {
            Console.WriteLine("\n*********************************");
            Console.WriteLine("**********    MENU     **********");
            Console.WriteLine("*********************************");
            Console.WriteLine("1a. Display details of households");
            Console.WriteLine("1b. Display grid of households");
            Console.WriteLine("2. Display details of companies");
            Console.WriteLine("3. Modify company");
            Console.WriteLine("4. Add new company");
            Console.WriteLine("6. Advance to next day");
            Console.WriteLine("7. Advance by a number of days");
            Console.WriteLine("Q. Quit");
            Console.Write("\n Enter your choice: ");
        }

        private void DisplayCompaniesAtDayEnd()
        {
            string details;
            Console.WriteLine("\n**********************");
            Console.WriteLine("***** Companies: *****");
            Console.WriteLine("**********************\n");
            foreach (var c in companies)
            {
                Console.WriteLine(c.GetName());
                Console.WriteLine();
                details = c.ProcessDayEnd();
                Console.WriteLine(details + "\n");
            }
        }

        private void ProcessAddHouseholdsEvent()
        {
            int NoOfNewHouseholds = rnd.Next(1, 5);
            for (int i = 1; i < NoOfNewHouseholds + 1; i++)
            {
                simulationSettlement.AddHousehold();
            }
            Console.WriteLine(NoOfNewHouseholds.ToString() + " new households have been added to the settlement.");
        }

        private void ProcessCostOfFuelChangeEvent()
        {
            decimal fuelCostChange = (decimal)( rnd.Next(1, 10) / 10.0);
            int upOrDown = rnd.Next(0, 2);
            int companyNo = rnd.Next(0, companies.Count);
            if (upOrDown == 0)
            {
                Console.WriteLine("The cost of fuel has gone up by " + fuelCostChange.ToString() + " for " + companies[companyNo].GetName());
            }
            else
            {
                Console.WriteLine("The cost of fuel has gone down by " + fuelCostChange.ToString() + " for " + companies[companyNo].GetName());
                fuelCostChange *= -1;
            }
            companies[companyNo].AlterFuelCostPerUnit(fuelCostChange);
        }

        private void ProcessReputationChangeEvent()
        {
            decimal reputationChange = (decimal) (rnd.Next(1, 10) / 10.0);
            int upOrDown = rnd.Next(0, 2);
            int companyNo = rnd.Next(0, companies.Count);
            if (upOrDown == 0)
            {
                Console.WriteLine("The reputation of " + companies[companyNo].GetName() + " has gone up by " + reputationChange.ToString());
            }
            else
            {
                Console.WriteLine("The reputation of " + companies[companyNo].GetName() + " has gone down by " + reputationChange.ToString());
                reputationChange *= -1;
            }
            companies[companyNo].AlterReputation(reputationChange);
        }

        private void ProcessCostChangeEvent()
        {
            // decimal costToChange = rnd.Next(0, 2); // why decimal?
            int costToChange = rnd.Next(0, 2); // this is clearer and avoids compiler warning
            int upOrDown = rnd.Next(0, 2);
            int companyNo = rnd.Next(0, companies.Count);
            decimal amountOfChange;
            if (costToChange == 0)
            {
                amountOfChange = (decimal) (rnd.Next(1, 20) / 10.0);
                if (upOrDown == 0)
                {
                    Console.WriteLine("The daily costs for " + companies[companyNo].GetName() + " have gone up by " + amountOfChange.ToString());
                }
                else
                {
                    Console.WriteLine("The daily costs for " + companies[companyNo].GetName() + " have gone down by " + amountOfChange.ToString());
                    amountOfChange *= -1;
                }
                companies[companyNo].AlterDailyCosts(amountOfChange);
            }
            else
            {
                amountOfChange = (decimal) (rnd.Next(1, 10) / 10.0);
                if (upOrDown == 0)
                {
                    Console.WriteLine("The average cost of a meal for " + companies[companyNo].GetName() + " has gone up by " + amountOfChange.ToString());
                }
                else
                {
                    Console.WriteLine("The average cost of a meal for " + companies[companyNo].GetName() + " has gone down by " + amountOfChange.ToString());
                    amountOfChange *= -1;
                }
                companies[companyNo].AlterAvgCostPerMeal(amountOfChange);
            }
        }

        private void DisplayEventsAtDayEnd()
        {
            Console.WriteLine("\n***********************");
            Console.WriteLine("*****   Events:   *****");
            Console.WriteLine("***********************\n");
            double eventRanNo;
            bool noChange = true; // MF changes here and below 18/12/2019

            eventRanNo = rnd.NextDouble();
            if (eventRanNo < 0.25)
            {
                eventRanNo = rnd.NextDouble();
                if (eventRanNo < 0.25)
                {
                    noChange = false; // MF
                    ProcessAddHouseholdsEvent();
                }
                eventRanNo = rnd.NextDouble();
                if (eventRanNo < 0.5)
                {
                    noChange = false; // MF
                    ProcessCostOfFuelChangeEvent();
                }
                eventRanNo = rnd.NextDouble();
                if (eventRanNo < 0.5)
                {
                    noChange = false; // MF
                    ProcessReputationChangeEvent();
                }
                eventRanNo = rnd.NextDouble();
                if (eventRanNo >= 0.5) // MF comment: why change the style here?
                {
                    noChange = false; // MF
                    ProcessCostChangeEvent();
                }
                /* MF block to ensure non events are reported */
                if (noChange)
                {
                    Console.WriteLine("No events.");
                }
                /* MF block */
            }
            else
            {
                Console.WriteLine("No events.");
            }
        }

        public void ProcessDayEnd()
        {
            decimal totalReputation = 0;
            List<decimal> reputations = new List<decimal>();
            int companyRNo, current, loopMax, x = 0, y = 0;
            foreach (var c in companies)
            {
                c.NewDay();
                totalReputation += c.GetReputationScore();
                reputations.Add(totalReputation);
            }
            loopMax = simulationSettlement.GetNumberOfHouseholds() - 1;
            for (int counter = 0; counter < loopMax + 1; counter++)
            {
                if (simulationSettlement.FindOutIfHouseholdEatsOut(counter, ref x, ref y))
                {
                    companyRNo = rnd.Next(1, Convert.ToInt32(totalReputation) + 1);
                    current = 0;
                    while (current < reputations.Count)
                    {
                        if (companyRNo < reputations[current])
                        {
                            companies[current].AddVisitToNearestOutlet(x, y);
                            break;
                        }
                        current++;
                    }
                }
            }
            DisplayCompaniesAtDayEnd();
            DisplayEventsAtDayEnd();
        }

        private void AddCompany()
        {
            int balance, x = 0, y = 0;
            string companyName, typeOfCompany = "9";
            Console.Write("Enter a name for the company: ");
            companyName = Console.ReadLine();
            Console.Write("Enter the starting balance for the company: ");
            balance = Convert.ToInt32(Console.ReadLine());
            while (typeOfCompany != "1" && typeOfCompany != "2" && typeOfCompany != "3")
            {
                Console.Write("Enter 1 for a fast food company, 2 for a family company or 3 for a named chef company: ");
                typeOfCompany = Console.ReadLine();
            }
            if (typeOfCompany == "1")
            {
                typeOfCompany = "fast food";
            }
            else if (typeOfCompany == "2")
            {
                typeOfCompany = "family";
            }
            else
            {
                typeOfCompany = "named chief";
            }
            simulationSettlement.GetRandomLocation(ref x, ref y);
            Company newCompany = new Company(companyName, typeOfCompany, balance, x, y, fuelCostPerUnit, baseCostForDelivery, simBank);
            companies.Add(newCompany);
        }

        public int GetIndexOfCompany(string companyName)
        {
            int index = -1;
            for (int current = 0; current < companies.Count; current++)
            {
                if (companies[current].GetName().ToLower() == companyName.ToLower())
                {
                    return current;
                }
            }
            return index;
        }

        public void ModifyCompany(int index)
        {
            string choice;
            int outletIndex, x, y;
            bool closeCompany;
            Console.WriteLine("\n*********************************");
            Console.WriteLine("*******  MODIFY COMPANY   *******");
            Console.WriteLine("*********************************");
            Console.WriteLine("1. Open new outlet");
            Console.WriteLine("2. Close outlet");
            Console.WriteLine("3. Expand outlet");
            Console.Write("\nEnter your choice: ");
            choice = Console.ReadLine();
            if (choice == "2" || choice == "3")
            {
                Console.Write("Enter ID of outlet: ");
                outletIndex = Convert.ToInt32(Console.ReadLine());
                if (outletIndex > 0 && outletIndex <= companies[index].GetNumberOfOutlets())
                {
                    if (choice == "2")
                    {
                        closeCompany = companies[index].CloseOutlet(outletIndex - 1);
                        if (closeCompany)
                        {
                            Console.WriteLine("That company has now closed down as it has no outlets.");
                            companies.RemoveAt(index);
                        }
                    }
                    else
                    {
                        companies[index].ExpandOutlet(outletIndex - 1);
                    }
                }
                else
                {
                    Console.WriteLine("Invalid outlet ID.");
                }
            }
            else if (choice == "1")
            {
                Console.Write("Enter X coordinate for new outlet: ");
                x = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter Y coordinate for new outlet: ");
                y = Convert.ToInt32(Console.ReadLine());
                if (x >= 0 && x < simulationSettlement.GetXSize() && y >= 0 && y < simulationSettlement.GetYSize())
                {
                    companies[index].OpenOutlet(x, y);
                }
                else
                {
                    Console.WriteLine("Invalid coordinates.");
                }
            }
            Console.WriteLine();
        }

        public void DisplayCompanies()
        {
            Console.WriteLine("\n*********************************");
            Console.WriteLine("*** Details of all companies: ***");
            Console.WriteLine("*********************************\n");
            foreach (var c in companies)
            {
                Console.WriteLine(c.GetDetails() + "\n");
            }
            Console.WriteLine();
        }

        public void Run()
        {
            string choice = "";
            int index;
            int runs;
            while (choice != "Q")
            {
                DisplayMenu();
                choice = Console.ReadLine();
                switch (choice)
                {
                    case "1a":
                        simulationSettlement.DisplayHouseholds();
                        break;
                    case "1b":
                        simulationSettlement.DisplayHouseholdGrid();
                        break;
                    case "2":
                        DisplayCompanies();
                        break;
                    case "3":
                        string companyName;
                        index = -1;
                        while (index == -1)
                        {
                            Console.Write("Enter company name: ");
                            companyName = Console.ReadLine();
                            index = GetIndexOfCompany(companyName);
                        }
                        ModifyCompany(index);
                        break;
                    case "4":
                        AddCompany();
                        break;
                    case "6":
                        ProcessDayEnd();
                        break;
                    case "7":
                        Console.Write("Enter number of days to run: ");
                        runs = Convert.ToInt32(Console.ReadLine());
                        int loop = 0;
                        while (loop < runs)
                        {
                            ProcessDayEnd();
                            loop++;
                        }
                        break;
                    case "Q":
                        Console.WriteLine("Simulation finished, press Enter to close.");
                        Console.ReadLine();
                        break;
                }
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Simulation thisSim = new Simulation();
            thisSim.Run();
        }
    }
}