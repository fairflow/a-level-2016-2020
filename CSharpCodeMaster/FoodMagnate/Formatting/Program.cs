﻿using System;
using System.Globalization;
 // beware, the workspace for this solution is 3080-S08_1 at least it seems to be.  
 // So the location on disk is...confusing at best
 // No, it seems it is not in that workspace after all.  So workspace/source code control is probably totally barfed :-(

namespace Formatting
{
    class Program
    {
        static void Main(string[] args)
        {
            var p1 = 42.397;
            var p2 = 1413.9;
            var q = DateTime.Now;
            const int maxDigits = 10;
            const string dp = "F2"; // this does not work
            Console.WriteLine($"The current currency symbol is {NumberFormatInfo.CurrentInfo.CurrencySymbol}");

            // you can use F2 (or F3, F4 &c) for 2 floating point digits to the right of the decimal point 
            // which is cleaner for a table output
            var s = $"The amounts are {p1,maxDigits:C2}, {p2,maxDigits:C2} and the date " +
                $"in exactly 20 days' time is {q.AddDays(20),10:d}; " +
                $"\nthe time would also be {q,6:t}";
            Console.WriteLine(s);
            Console.WriteLine("Goodbye World!");
        }
    }
}
