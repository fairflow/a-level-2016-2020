﻿using System;
using System.Collections.Generic;

namespace Queues
{
    class QueueTypes
    {
        public class CircularQueue<Type> // A class implementing a circular queue
        {// we can have queues of many different types: hence the <Type>
            private int maxSize;
            private int currentSize;
            private int front;
            private int back;
            private Type[] queue;

            public CircularQueue(int max)
            {
                Console.WriteLine("New queue of size {0} created", max);
                queue = new Type[max];
                maxSize = max;
                currentSize = 0;
                front = 0;
                back  = 0; // back points to the next free location
            }

            public void enQueue(Type item)
            {
                if (!isFull())
                {
                    currentSize++;
                    queue[back] = item; // back points to the next free location
                    back = (back + 1) % maxSize;
                }
                else Console.WriteLine("Queue full.  Sorry!");
            }

            public Type deQueue()
            {
                if (!isEmpty())
                {
                    var item = queue[front];
                    front = (front + 1) % maxSize;
                    currentSize--;
                    return item;
                }
                else
                {
                    Console.WriteLine("Queue empty.  What to do?");
                    return default; // in C# every type has a default value
                }
            }

            public bool isEmpty()
            {
                if (currentSize == 0)
                { return true; }
                else
                { return false; }
            }

            public bool isFull()
            {
                if (currentSize >= maxSize)
                { return true; }
                else
                { return false; }
            }

            public Type[] getItems()
            {
                return queue; // Exercise: what is wrong with this?
            }

            public void printItems()
            {
                for (var i = front; i != back; i = (i+1) % maxSize)
                { Console.WriteLine(queue[i]); }
            }
        }

        public class MyQueue<Type> // we can have queues of many different types: hence <Type>
        {
            private Queue<Type> innerQ; // we build on the built-in Queue datatype
            private int limit; // to be used to limit size of queue
                               //// all these properties are private and not intended for direct use
            private int size;  //// the number of items in the queue

            // Constructors //

            //// create a new queue of given maximum length
            public MyQueue(int max)
            {
                innerQ = new Queue<Type>();

                limit = max;
            }

            //// create a new queue of given length from an array of items
            public MyQueue(int max, Type[] items) // we cannot use Array directly
            {
                innerQ = new Queue<Type>(items);
                limit = max;
                size = items.Length; // Arrays have a Length property
            }

            // Methods //

            public void enQueue(Type item)
            { size++; innerQ.Enqueue(item); }

            public Type deQueue()
            { size--; return innerQ.Dequeue(); }

            public bool isEmpty()
            {
                if (innerQ.Count == 0) // Built-in Queues have a Count property
                { return true; }
                else
                { return false; }
            }

            public bool isFull()
            {
                if (innerQ.Count >= limit)
                { return true; }
                else
                { return false; }
            }

            public Type[] getItems()
            { return innerQ.ToArray(); }
        }

        public static void Main()
        {
            Console.WriteLine("Demonstrating own Queue datatype\n");

            MyQueue<string> animals = new MyQueue<string>(5);

            animals.enQueue("manatee");
            animals.enQueue("pig");
            animals.enQueue("chicken");
            animals.enQueue("fruit-fly");
            animals.enQueue("alligator");

            Console.WriteLine("Using our method for getting all items for enumeration:");
            foreach (string animal in animals.getItems())
            {
                Console.WriteLine(animal);
            }

            Console.WriteLine("\nTesting deQueue\n");
            Console.WriteLine("Dequeuing '{0}'", animals.deQueue());
            Console.WriteLine("Dequeuing '{0}'", animals.deQueue());

            if (animals.isEmpty())
            {
                Console.WriteLine("Animal queue is empty");
            }
            else
            if (animals.isFull())
            {
                Console.WriteLine("Animal queue is full");
            }
            else
            {
                Console.WriteLine("Animal queue is just fine");
            }

            animals.enQueue("pigeon");
            animals.enQueue("diplodocus");
            Console.WriteLine("\nInspect queue again:");
            foreach (string animal in animals.getItems())
            {
                Console.WriteLine(animal);
            }

            if (animals.isEmpty())
            {
                Console.WriteLine("Animal queue is empty");
            }
            else
            if (animals.isFull())
            {
                Console.WriteLine("Animal queue is full");
            }
            else
            {
                Console.WriteLine("Animal queue is just fine");
            }

            Console.WriteLine("\nNow dequeue all items...");
            animals.deQueue();
            animals.deQueue();
            animals.deQueue();
            animals.deQueue();
            animals.deQueue();
            if (animals.isEmpty())
            {
                Console.WriteLine("Animal queue is empty");
            }
            else
            if (animals.isFull())
            {
                Console.WriteLine("Animal queue is full");
            }
            else
            {
                Console.WriteLine("Animal queue is just fine");
            }

            Console.WriteLine("\nTesting creating a queue from an array:");

            MyQueue<string> characters =
                new MyQueue<string>(10, new string[] { "a", "b", "c", "d", "e" });
            foreach (string character in characters.getItems())
            {
                Console.WriteLine(character);
            }

            Console.WriteLine("\nDone with animal and character queues now!");

            Console.WriteLine("\nNow to testing a circular queue...");
            var printQueue = new CircularQueue<string>(5);
            printQueue.enQueue("J45");
            printQueue.enQueue("J38");
            printQueue.enQueue("J92");
            Console.WriteLine("Get all items for enumeration:");
            foreach (string job in printQueue.getItems())
            {
                Console.WriteLine(job);
            }

            Console.WriteLine("\nTesting deQueue\n");
            Console.WriteLine("Dequeuing '{0}'", printQueue.deQueue());
            Console.WriteLine("Dequeuing '{0}'", printQueue.deQueue());
            Console.WriteLine("Dequeuing '{0}'", printQueue.deQueue());

            printQueue.enQueue("J44");
            printQueue.enQueue("J55");
            printQueue.enQueue("J66");
            printQueue.enQueue("J77");

            Console.WriteLine("Dequeuing '{0}'", printQueue.deQueue());

            Console.WriteLine("Get all items for enumeration:");
            foreach (string job in printQueue.getItems())
            {
                Console.WriteLine(job);
            }
            Console.WriteLine("Print all items:");
            printQueue.printItems();

            Console.WriteLine("\nFinished with Queues altogether!");
        }
    }
}
